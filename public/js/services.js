angular.module('StaticRealtimeBlog')

//article service
.service('ArticleService', function($http){
  //save an edit
   this.saveArticleEdit = function(article){
     //turn tags into array
     console.log(article.tags);
     if(article.tags.length > 0){
     }else if(article.tags.length > 2){
       alert("You can only add up to 3 tags!");
       return;
     }else{
       article.tags = typeof(article.tags) === 'undefined' ? "" : article.tags.split(/\s*,\s*/);
     }

     $http.put('http://api.blavity.com/v1/articles/', {article: article} ).then(function(response){
       var article = response.data;

       return article;

     });
   };//end saveArticleEdit


   this.publishArticle = function(article){
     //toggle the article's pusblished state
     article.published = !article.published;
     //then just send as an update on this article
     $http.put('http://api.blavity.com/v1/articles/', {article: article} ).then(function(response){

       var article = response.data;

       return article;

     });
   };//end publishArticle

   this.submitArticle = function(article){
     //toggle the article's submitted state
     article.submitted = !article.submitted;
     //then just send as an update on this article
     $http.put('http://api.blavity.com/v1/articles/', {article: article} ).then(function(response){

       var article = response.data;

       return article;

     });
   };//end publishArticle
   //saves the article by posting it to the api save endpoint in the correct format
   this.createArticle = function(article){
     //turn tags into array
     if(article.tags.length > 0){
     }else{
       article.tags = typeof(article.tags) === 'undefined' ? "" : article.tags.split(/\s*,\s*/);
     }
     // console.log(article.tags);

     $http({
         method: 'POST',
         url: 'http://api.blavity.com/v1/articles',
         //IMPORTANT!!! You might think this should be set to 'multipart/form-data'
         // but this is not true because when we are sending up files the request
         // needs to include a 'boundary' parameter which identifies the boundary
         // name between parts in this multi-part request and setting the Content-type
         // manually will not set this boundary parameter. For whatever reason,
         // setting the Content-type to 'false' will force the request to automatically
         // populate the headers properly including the boundary parameter.
         headers: { 'Content-Type': undefined },
         //This method will allow us to change how the data is sent up to the server
         // for which we'll need to encapsulate the model data in 'FormData'
         transformRequest: function (data) {
             var formData = new FormData();
             //need to convert our json object to a string version of json otherwise
             // the browser will do a 'toString()' on the object which will result
             // in the value '[Object object]' on the server.
             formData.append("article", angular.toJson(article));
             //now add all of the assigned files
             for (var i = 0; i < $scope.files.length; i++) {
                 //add each file to the form data and iteratively name them
                 formData.append("files", $scope.files[i]);
             }
             return formData;
         },
         //Create an object that contains the model and files which will be transformed
         // in the above transformRequest method
         data: { article: article, files: $scope.files }
     }).
     success(function (data, status, headers, config) {

         $scope.article = null;
     }).
     error(function (data, status, headers, config) {
         alert("failed!");
     });


   };//end createArticle

})//end article service




.service('AuthService', function($q, $http, API_ENDPOINT) {
  var LOCAL_TOKEN_KEY = 'yourTokenKey';
  var isAuthenticated = false;
  var authToken;

  function loadUserCredentials() {
    var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
    if (token) {
      useCredentials(token);
    }
  }

  function storeUserCredentials(token) {
    window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
    useCredentials(token);
  }

  function useCredentials(token) {
    isAuthenticated = true;
    authToken = token;

    // Set the token as header for your requests!
    $http.defaults.headers.common.Authorization = authToken;
  }

  function destroyUserCredentials() {
    authToken = undefined;
    isAuthenticated = false;
    $http.defaults.headers.common.Authorization = undefined;
    window.localStorage.removeItem(LOCAL_TOKEN_KEY);
  }

  var register = function(user) {
    return $q(function(resolve, reject) {
      $http.post(API_ENDPOINT.url + '/signup', user).then(function(result) {
        if (result.data.success) {
          resolve(result.data.msg);
        } else {
          reject(result.data.msg);
        }
      });
    });
  };

  var login = function(user) {
    return $q(function(resolve, reject) {
      $http.post(API_ENDPOINT.url + '/authenticate', user).then(function(result) {
        if (result.data.success) {
          storeUserCredentials(result.data.token);
          resolve(result.data.msg);
        } else {
          reject(result.data.msg);
        }
      });
    });
  };

  var logout = function() {
    destroyUserCredentials();
  };

  loadUserCredentials();

  return {
    login: login,
    register: register,
    logout: logout,
    isAuthenticated: function() {return isAuthenticated;},
  };
})

.factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
  return {
    responseError: function (response) {
      $rootScope.$broadcast({
        401: AUTH_EVENTS.notAuthenticated,
      }[response.status], response);
      return $q.reject(response);
    }
  };
})

.config(function ($httpProvider) {
  $httpProvider.interceptors.push('AuthInterceptor');
});
