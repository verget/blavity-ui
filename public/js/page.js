'use strict';
var APIURL = "http://api.blavity.com/v1/";
//var APIURL = "http://localhost:3100/v1/";
/*//testing server token
var Token = 'JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1N2E3ZWZjNjQ2YmVlOWVjN2Q2NjI4ZjkiLCJlbWFpbCI6Im1lZ2hhbmEudGVzdEBibGF2aXR5LmNvbSIsInBhc3N3b3JkIjoiJDJhJDEwJEVKMHZveGpETjFXMThXMW5WZ2VDdE9hTU1FYUhtVmMycUk3aVFtYWlxZE01MFJvZzFvTXVLIiwidXNlcm5hbWUiOiJtZWdoYW5hdGVzdCIsIl9fdiI6MCwidXBkYXRlZF9hdCI6IjIwMTYtMDgtMDhUMDI6MzQ6NDYuMzA5WiIsImxpbmtzIjpbXX0.KglPiHqRFEQOaNnCh9juvJvDFAA_o4mVJuGKRsYbHAY';
//testing local token
var Token = 'JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1N2E3ZmQ4ZTViYzg3Nzk0MzQzOGIzNWYiLCJlbWFpbCI6Im1lZ2hhbmEudGVzdEBibGF2aXR5LmNvbSIsInBhc3N3b3JkIjoiJDJhJDEwJFdKOU1GcThPd3J0akhBamJtNVZyZnV4WGlMZ3RDa1hCa3NoUDQueXNiS1djOHcvZ2h5SUltIiwidXNlcm5hbWUiOiJtZWdoYW5hdGVzdCIsIl9fdiI6MCwidXBkYXRlZF9hdCI6IjIwMTYtMDgtMDhUMDM6MzM6MzQuMzY3WiIsImxpbmtzIjpbXX0.RE73dmT_y-BNX3wHbAl4oygCjPR-fb3eP-WOsCrDfK8';
**/
var app = angular.module("StaticRealtimeBlog", ['ngAnimate', 'ngCookies', 'ngSanitize', 'angulike', 'angular-loading-bar', 'ui.select2', 'slugifier']).run([
    '$rootScope','$cookies','apiRequest',
    function($rootScope,$cookies,apiRequest) {
        $rootScope.facebookAppId = '567482886746994'; // set your facebook app id here
        $rootScope.user='';
        $rootScope.accessToken=$cookies.get('token');
         $rootScope.accessToken=undefined
        console.log($rootScope.accessToken)
        $rootScope.isLogin=false;
        $rootScope.loader=false;
        $rootScope.isUserLogin=function () {
                $rootScope.accessToken=$cookies.get('token');
                if($rootScope.accessToken!==undefined || $rootScope.accessToken!==undefined ){
                    $rootScope.isLogin=true;
                    return true;
                }
                else{
                    $rootScope.isLogin=false;
                    return false;
                }
          }
          $rootScope.isLogin= $rootScope.isUserLogin();
          $rootScope.logout=function(){
              $cookies.remove("userInfo");
              location.reload();
          }
          $rootScope.getUserInfo=function () {
              $rootScope.isLogin=false;
              if($cookies.get('token')!==undefined  || $rootScope.accessToken!==undefined){
                    apiRequest.getUserInfo().success(function(data, status, headers, config) {
                        console.log(data)
                        $rootScope.user=data.user;
                        $rootScope.isLogin=true;
                    }).error(function(data, status, headers, config) {
                        console.log(data);
                        $cookies.remove('token');
                        $rootScope.isLogin=false;
                        //alert(data.msg)
                    });
              }
              else{
                $rootScope.isLogin=false;
              }

          }

    }
]);




app.filter('urlSlug', function() {
    return function(val) {
      if (val){
        var slug = val.toLowerCase().replace(/\s+/g, "-"); //replace spaces with dashes
        //remove any other weird characters with
        return slug.replace(/[^a-zA-Z0-9-_]/g, '');
      }//end if

    };
});

app.controller("headerCTL", function($scope, $http, $filter, $cookies, $sce,$rootScope,apiRequest) {
    $scope.userdata='';
   if($cookies.get('token')!==undefined  || $rootScope.accessToken!==undefined){
        apiRequest.getUserInfo().success(function(data, status, headers, config) {
            console.log(data)
            $scope.userdata=data.user;
            $rootScope.isLogin=true;
        }).error(function(data, status, headers, config) {
            console.log(data);
            $cookies.remove('token');
            $rootScope.isLogin=false;
            //alert(data.msg)
        });
    }
    //for displaying in header
    $scope.todaysDate = new Date();
})

// so we can display html as html and not text... you know what i mean. unescaped
app.filter('unsafe', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
});


app.filter('htmlToPlaintext', function() {
    return function(text) {
        return text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };
});

app.filter('truncatePostContent', function() {
    return function(content, maxLength) {
        var content = content ? String(content).replace(/<[^>]+>/gm, '') : ''; //remove html tags from string
        //trim the string to the maximum length
        var trimmedString = content.substr(0, maxLength);
        //re-trim if we are in the middle of a word
        return trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" "))) + "...";
    };
});


app.filter('embedvideo', function() {
    return function(content, maxLength) {
        if(content.includes("://youtu") || content.includes("://www.youtu")){
            content=content.replace("watch?v=", "v/")
            var v=content.split('/');
            return v[0]+'//youtube.com/v/'+v[v.length-1];
        }
        else{
            return content;
        }

    };
});


app.filter('linkify', function() {
    return function (text) {
      if(text){
        var urlRegex =/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        return text.replace(urlRegex, function(url) {
            return '<a href="' + url + '">' + url + '</a>';
        });
      }//end if
    };
});


app.filter('hash', function() {
    return function(val) {
        if (typeof val === 'undefined' || val.trim() === '')
            return '';

        return md5(val);
    };
});


app.filter('timeago', function() {
    return function(input, p_allowFuture) {

        var substitute = function (stringOrFunction, number, strings) {
                var string = angular.isFunction(stringOrFunction) ? stringOrFunction(number, dateDifference) : stringOrFunction;
                var value = (strings.numbers && strings.numbers[number]) || number;
                return string.replace(/%d/i, value);
            },
            nowTime = (new Date()).getTime(),
            date = (new Date(input)).getTime(),
            //refreshMillis= 6e4, //A minute
            allowFuture = p_allowFuture || false,
            strings= {
                prefixAgo: '',
                prefixFromNow: '',
                suffixAgo: "ago",
                suffixFromNow: "from now",
                seconds: "less than a minute",
                minute: "about a minute",
                minutes: "%d minutes",
                hour: "about an hour",
                hours: "about %d hours",
                day: "a day",
                days: "%d days",
                month: "about a month",
                months: "%d months",
                year: "about a year",
                years: "%d years"
            },
            dateDifference = nowTime - date,
            words,
            seconds = Math.abs(dateDifference) / 1000,
            minutes = seconds / 60,
            hours = minutes / 60,
            days = hours / 24,
            years = days / 365,
            separator = strings.wordSeparator === undefined ?  " " : strings.wordSeparator,


            prefix = strings.prefixAgo,
            suffix = strings.suffixAgo;

        if (allowFuture) {
            if (dateDifference < 0) {
                prefix = strings.prefixFromNow;
                suffix = strings.suffixFromNow;
            }
        }

        words = seconds < 45 && substitute(strings.seconds, Math.round(seconds), strings) ||
        seconds < 90 && substitute(strings.minute, 1, strings) ||
        minutes < 45 && substitute(strings.minutes, Math.round(minutes), strings) ||
        minutes < 90 && substitute(strings.hour, 1, strings) ||
        hours < 24 && substitute(strings.hours, Math.round(hours), strings) ||
        hours < 42 && substitute(strings.day, 1, strings) ||
        days < 30 && substitute(strings.days, Math.round(days), strings) ||
        days < 45 && substitute(strings.month, 1, strings) ||
        days < 365 && substitute(strings.months, Math.round(days / 30), strings) ||
        years < 1.5 && substitute(strings.year, 1, strings) ||
        substitute(strings.years, Math.round(years), strings);
        console.log(prefix+words+suffix+separator);
        prefix.replace(/ /g, '')
        words.replace(/ /g, '')
        suffix.replace(/ /g, '')
        return (prefix+' '+words+' '+suffix+' '+separator);

    };
});

//this adds the contenteditable directive so that we can use in-place editing with angular's model binding
// app.directive("contenteditable", function() {
//     return {
//         restrict: "A",
//         require: "?ngModel",
//         link: function(scope, element, attrs, ngModel) {
//             if (!ngModel) {
//                 return;
//             } // do nothing if no ng-model
//
//             ngModel.$render = function() {
//                 element.html(ngModel.$viewValue);
//             };
//
//             element.on('blur keyup change', function() {
//                 scope.$evalAsync(read);
//             });
//             ngModel.$render();
//
//             function read() {
//                 var html = element.html();
//                 // When we clear the content editable the browser leaves a <br> behind
//                 // If strip-br attribute is provided then we strip this out
//                 if (attrs.stripBr && html == '<br>') {
//                     html = '';
//                 }
//                 ngModel.$setViewValue(html);
//             }
//         }
//     };
// });


// app.directive('isInView', function() {
//   return {
//     restrict: 'AE',
//     replace: false,
//     link: function(scope, elem, attrs) {

//       $document.bind('scroll', function () {
//           // scope.$apply(attrs.scrolly);
//           alert("scrolled");
//       });


//       elem.bind('click', function() {
//         // if(elem.offset().top <= 100){
//           elem.css('background-color', 'blue');
//           scope.$apply(function() {
//             scope.color = "grey";
//           });
//         // }

//       });
//       elem.bind('mouseover', function() {
//         elem.css('cursor', 'pointer');
//       });
//     }
//   };
// });




// app.directive('renderIframely', ['$timeout', function ($timeout) {
//
//     return {
//         link: function ($scope, element, attrs) {
//             $timeout(function () {
//
//                 var eblink = $(element).html();
//                 //encode the uri
//
//                 alert(eblink);
//                 // return;
//
//                 eblink = encodeURI(eblink);
//
//                 var url = 'http://iframe.ly/api/oembed?url='+ eblink +'&api_key=a9fe47b252203d403c24dc';
//
//                 $http.get(url).then(function(res) {
//                   $scope = angular.element(element).scope();
//                   $scope.fullArticles[0].embedhtml = $sce.trustAsHtml(res.data.html);
//
//                 });
//
//                 window.iframely && iframely.load();
//             }, 0, false);
//         }
//   };
// }]);
app.directive("renderIframely", function( $timeout,$http,$sce, $compile){

    return {
        template: "<div ng-bind-html='htmls'></div>",
        replace: true,
        restrict: 'A',
        scope: true, //default
        link: function(scope, element, attrs){
            attrs.$observe('pageHref', function(newValue) {
                var content=newValue;
                 $timeout(function () {
                    var url='';
                    if(content.includes("://youtu") || content.includes("://www.youtu")){
                        content=content.replace("watch?v=", "v/")
                        var v=content.split('/');
                        url= 'http://youtube.com/embed/'+v[v.length-1];
                    }
                    else{
                        url= content;
                    }
                    var eblink = url;//$(element).html();
                    //encode the uri
                    // return;

                    eblink = encodeURI(eblink);

                    var url = 'http://iframe.ly/api/oembed?url='+ eblink +'&api_key=a9fe47b252203d403c24dc';

                    $http.get(url).then(function(res) {
                    scope = angular.element(element).scope();
                    scope.htmls = $sce.trustAsHtml(res.data.html);
                    $compile(element.contents())(scope)
                    });

                    window.iframely && iframely.load();
                }, 0, false);

             });
        }
    }
});

app.directive('fileUpload', function() {
    return {
        scope: true, //create a new scope
        link: function(scope, el, attrs) {
            el.bind('change', function(event) {
                var files = event.target.files;
                //iterate files since 'multiple' may be specified on the element
                for (var i = 0; i < files.length; i++) {
                    //emit event upward
                    scope.$emit("fileSelected", {
                        file: files[i]
                    });
                }
            });
        }
    };
});

app.config(function($logProvider){
    $logProvider.debugEnabled(true);
});

app.config( ['$compileProvider', function( $compileProvider ) {
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|sms):/);
        // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
    }
]);

app.config(["$httpProvider", function($httpProvider) {
    $httpProvider.interceptors.push(function($q, $location, $cookies) {
        return {
            response: function(response) {
                // do something on success

                if (response.data) {
                    if (response.data.token) {
                        if (response.data.success == true) {
                            // if(response.data.success == true){
                            $cookies.put('token', response.data.token);
                            console.log($cookies.getAll());
                            // window.localStorage.token = response.data.token;
                        } else {
                            // console.log(response.data.msg);
                            // alert("Sorry, sign in failed for some reason!");
                            // window.location = "http://jordancryingface.com";
                        }
                    } //end if response has token
                } //end if response has data

                return response;
            },
            responseError: function(response) {
                if (response.status === 401)
                    alert("401!");

                if (response.status === 403)
                    alert("403!");

                return $q.reject(response);
            }

        };

        return {
            'request': function(config) {
                config.headers['token'] = $cookies.get('token');
                return config;
            }
        };

    });

}]);


// //app.filter('doshortcode', function($scope) {console.log($scope) });
//
// app.directive('dynFbCommentBox', function() {
//     function createHTML(href, numposts, colorscheme, width) {
//         return '<div class="fb-comments" ' +
//             'data-href="' + href + '" ' +
//             'data-numposts="' + numposts + '" ' +
//             'data-colorsheme="' + colorscheme + '" ' +
//             'data-width="' + width + '">' +
//             '</div>';
//     }
//
//
//     return {
//         restrict: 'A',
//         scope: {},
//         link: function postLink(scope, elem, attrs) {
//             attrs.$observe('pageHref', function(newValue) {
//                 var href = newValue;
//                 var numposts = attrs.numposts || 5;
//                 var colorscheme = attrs.colorscheme || 'light';
//                 var width = attrs.width || '100%';
//                 elem.html(createHTML(href, numposts, colorscheme, width));
//                 FB.XFBML.parse(elem[0]);
//             });
//         }
//     };
// });

 //app.directive('fbShare', [
 //    function() {
 //        return {
 //            restrict: 'A',
 //            scope: false,
 //            // scope: { article: '=' },
 //            link: function(scope, element) {
 //                element.on('click', function() {
 //                    FB.ui({
 //                        method: 'feed',
 //                        name: fullArticle.title,
 //                        link: 'http://preview.blavity.com/' + fullArticle.slug,
 //                        picture: fullArticle.wp_featuredImage,
 //                        caption: fullArticle.excerpt,
 //                        // name: article.title,
 //                        // link: 'http://preview.blavity.com/'+article.slug,
 //                        // picture: article.wp_featuredImage,
 //                        // caption: article.excerpt,
 //                        description: 'Blavity is an awesome site, with more stuff like this',
 //                        // message: 'Message you want to show'
 //                    });
 //                });
 //            }
 //        };
 //    }
 //]);

// app.directive('dynFbLikes', function() {
//     function createHTML(href) {
//         return '<i class="fa fa-thumbs-up fb-like-button" data-href="http://preview.blavity.com/' + href + '" data-layout="standard" data-action="like"></i>';
//     }
//
//     return {
//         restrict: 'A',
//         scope: {},
//         link: function postLink(scope, elem, attrs) {
//             attrs.$observe('pageHref', function(newValue) {
//                 var href = newValue;
//                 elem.html(createHTML(href));
//                 FB.XFBML.parse(elem[0]);
//             });
//         }
//     };
// });


// app.directive('parseFBML', function() {
//   return function(scope, elem) {
//     // if (scope.$last){
//       // scope.$emit('LastRepeaterElement');
//       //refresh fb comment boxes
//       console.log("this comment box: ");
//       console.log(angular.element(elem));
//       FB.XFBML.parse(angular.element(elem));
//     // }
//   };
// });

// app.directive("scrollUrl", function ($window) {
//     return function(scope, element, attrs) {
//         angular.element($window).bind("scroll", function() {
//           console.log("scrolled");
//
//             scope.$apply();
//         });
//     };
// });

// app.directive("scrollUrl", function ($window){
//   return {
//       restrict: "A",
//       scope: {fullArticle:'=article'},
//       link: function (scope, element, attrs) {
//
//         console.log("B!");
//         console.log("fullArticle: ");
//         console.log(JSON.stringify(scope.fullArticle));
//
//         angular.element($window).bind("click", function() {
//           // angular.element($window).bind("scroll", function() {
//             console.log("A!");
//               scope.fullArticle.pageYOffset = this.pageYOffset;
//               var ceil = this.pageYOffset;
//               var floor = this.prop('offsetHeight');
//               console.log(scope.fullArticle);
//               // console.log(scope.fullArticles[i]);
//               //if the article is in view
//               if (scope.fullArticle.pageYOffset < ceil && scope.fullArticle.pageYOffset > floor) {
//                   //show us it's slug
//                   alert(scope.fullArticle.slug);
//                   console.log("in view: " + scope.fullArticle.slug);
//               }//end if
//             scope.$apply();
//           });
//
//       }
//   };
//
// });

app.directive('stickyicky', function ($window, $timeout) {
    return {
      link: function (scope, element, attrs) {
        // link: $timeout(function (scope, element, attrs) {

          var lifespan = $(element).parent().parent().parent().parent().parent().height();
          // var lifespan = angular.element(element).parent().parent().parent().parent().parent().height();
          var reached = false;
          var visible = true;
          var distance;
          // var rect = $(element).getBoundingClientRect();
          var rect = angular.element(element)[0].getBoundingClientRect();

          angular.element($window).bind("scroll", function() {
            var left = rect.left;

            if(reached != true){
              // if( typeof distance !== 'undefined'){
                distance = $(element).offset().top;
              // }
            }

            if (this.pageYOffset >= distance) {

              reached = true;

              element.addClass("stickyicky");
              // $(element).css("left", left);
              console.log('should be stickin.');

              if(visible == true && this.pageYOffset >= lifespan + distance){
                visible = false;
                element.fadeOut("slow", function(){
                  element.removeClass("stickyicky");
                  console.log('should be slidin.');
                });

              }//end if

             } else {
               element.removeClass("stickyicky");
              //  element.css("left", 0);

                console.log('should be slidin.');

             }//end else
            scope.$apply();
          });


        }
        // })
  };
});



//end get embed urls using oembed

app.controller("AdCtrl", function($scope, $http, $filter, $cookies, $sce, $timeout) {
  $scope.sideBarHasAd = true;

  // //googletag.addService(googletag.pubads());
  //
  // googletag.pubads().addEventListener('slotRenderEnded', function(event) {
  //     if (event.slot.getSlotElementId() == "div-gpt-ad-300x250_MediumRectangleATF") {
  //         $scope.sideBarHasAd = !event.isEmpty;
  //     }
  // });
});


app.controller("ArticleCtrl", function($scope, $http, $filter, $cookies, $sce, $timeout, $compile) {
    //an array of files selected
    $scope.files = [];

    //listen for the file selected event
    $scope.$on("fileSelected", function(event, args) {
        $scope.$apply(function() {
            //add the file object to the scope's files collection
            $scope.files.push(args.file);
        });
    });
    $scope.TrustDangerousSnippet = function(post) {
        var myReg = /\[.+\]/g;
        // console.log(post);
        //paragraphText = paragraphText.replace(myReg, '');
        return $sce.trustAsHtml(post.replace(/\[{1}[^\]]+\]{1}/ig, ""));
        //return $sce.trustAsHtml(post);
    };

    $scope.featuredArticle = null;
    $scope.lastArticle = {};

    $scope.$watch("leadingArticleId", function() {
        $scope.lastArticle._id = $scope.leadingArticleId;
    });

    $scope.infinite_articles = [];
    $scope.fullArticles = [];
    $scope.more_articles = [];
    $scope.editMode = false;
    $scope.newArticleCount = 0;
    $scope.searchResults = [];
    $scope.searchQuery = '';
    $scope.user = null;
    $scope.page_position = 0;
    $scope.npArticlesLoaded = [];
    $scope.theAuthor = null;
    $scope.random = false;
    $scope.order = 0;

    if(window.location.pathname.trim() == "/"){
      $scope.offset = 15;
    }else{
      $scope.offset = 8;
    }

    //if out if this device is an iOS devices
    //so we know which sms link format to use for sharing
    $scope.isIOS = function() {

      var iDevices = [
        'iPad Simulator',
        'iPhone Simulator',
        'iPod Simulator',
        'iPad',
        'iPhone',
        'iPod'
      ];


      if (!!navigator.platform) {
        while (iDevices.length) {
          if (navigator.platform === iDevices.pop()){ return true; }
        }
      }

      return false;
    };

    $scope.chunk = function (arr, size) {
      var newArr = [];
      for (var i=0; i<arr.length; i+=size) {
        newArr.push(arr.slice(i, i+size));
      }
      return newArr;
    }

    $scope.isMobile = function (){
      if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        // tasks to do if it is a Mobile Device
        return true;
      }//end if

      return false;
    };

    // //sockets - realtime updates
    //       //open up a socket.io for emitting and listening to events through sockets
    //       // var socket = io('http://45.55.30.92:3000');
    //       //when the acrticle created event is detected
    //       socket.on('article created', function(resp){
    //         // $scope.$apply();//so angular will refresh the view and add our flash message
    //       });//end on article published
    //
    //       socket.on('article published', function(resp){
    //         console.log(resp.article._id);
    //         $scope.npArticlesLoaded = [];
    //         $scope.$apply();//so angular will refresh the view and add our flash message
    //         //get the article that triggered the event using the api by it's slug
    //         // $http.get('' + resp.article.slug ).then(function(rsp){
    //         //   $scope.fullArticles.push(rsp.data);
    //         //   console.log(rsp.data);
    //         // });//end get article by slug
    //       });//end on article published
    // //end sockets


    //page initialization
    //get a random article to be featured on the 'homepage'
    // $http.get("articles/1").then(function( res ){
    //   $scope.featuredArticle = res.data[0];
    //   // console.log(res.data[0]);
    // });

    $scope.getRandomArticle = function(range) {
        // range ex. '2016-1-1, 2016-5-1'
        var range = typeof(range) === "undefined" ? "" : range;
        $http.get(APIURL + "articles/any/" + range).then(function(res) {
            console.log(res.data.title);
            return res.data;
        });
    };


        $scope.loadMoreArticles = function(count, chunk, category){
          var count = count ? parseInt(count) : 5;
          var chunk = chunk ? parseInt(chunk) : 0;
          var path = category ? "articles/categories/"+category+"/"+count+"/" + $scope.offset : "articles/"+count+"/" + $scope.offset;

          $http.get(APIURL+path).then(function( res ){
            //increase the offset

            $scope.offset += count;

            //order each of the five articles
            for (var i = res.data.length - 1; i >= 0; i--) {
              res.data[i].order = $scope.order;
              $scope.order++;
              //push each of the articles onto the more articles array
              $scope.more_articles.push(res.data[i]);
            };

            $scope.lastArticle = $scope.more_articles[$scope.more_articles.length - 1];


            //if the chunk param is defined
            if(chunk){
              //chunk the articles so we can display them in rows
              $scope.more_articles = $scope.chunk($scope.more_articles, chunk);
            }//end if chunk


            $timeout(function () {
              //refresh google ads

              googletag.cmd.push(function() {
                  var mapping1 = googletag.sizeMapping().
                    addSize([1024, 768], [728, 90]).
                    addSize([980, 690], [728, 90]).
                    addSize([640, 480], [300, 250], [320, 50]).
                    addSize([0, 0], [300, 250], [320, 50]).
                    build();

                  //Adslot 4 declaration
                  var slotC = googletag.defineSlot('/63052033/bla', [[728,90],[300,250], [320,50]], 'adSlotC-'+$scope.lastArticle._id).defineSizeMapping(mapping1).
                  setCollapseEmptyDiv(true).setTargeting('pos',['infinite']).setTargeting('page',['home']).addService(googletag.pubads());

                 // Display has to be called before
                 // refresh and after the slot div is in the page.
                 googletag.display('adSlotC-'+$scope.lastArticle._id);
                 googletag.pubads().refresh([slotC]);
               });


            }, 1000);


          });
        };



//get embed urls using oembed

// $timeout(function () {
//   var embed_urls = $('.embed-url');
//   console.log(embed_urls);
//
//   for (var i = 0; i < embed_urls.length; i++) {
//     var eblink = $(embed_urls[i]).html();
//     //encode the uri
//     console.log(eblink);
//
//     alert(eblink);
//     eblink = encodeURI(eblink);
//     alert(eblink);
//
//
//     // var url = 'http://iframe.ly/api/oembed?url='+ eblink +'&api_key=a9fe47b252203d403c24dc';
//     //
//     // $http.get(url).then(function(res) {
//     //   $scope = angular.element(embed_urls[i]).scope();
//     //   $scope.fullArticles[i].embedhtml = $sce.trustAsHtml(res.data.html);
//     //
//     // });
//
//   }
//
// }, 2000);


//end get embed urls using oembed



    //if this is the homepage
    //when the window loads, ajax in a set of articles
    if( window.location.pathname.trim() == "/" ){
      $scope.loadMoreArticles();
    }
    //end page initialization

    $scope.loadComments = function(article) {

        $http.get(APIURL + "articles/comments/" + article._id + "/20").then(function(res) {
            console.log(res.data);
            article.comments = (res.data);
        });
    };

    for (var i = $scope.fullArticles.length - 1; i >= 0; i--) {
        $scope.loadComments($scope.fullArticles[i]);
    };


    $scope.likeArticle = function(article) {
        //make likes an empty array if one doesn't already exist on the article
        article.likes = typeof(article.likes === 'undefined') ? [] : article.likes;
        article.likes.push("user1234");
        //todo://push the actual user id
        return;

    };

    /*Infinte Scroll*/
    /*Infinte Scroll*/
    /*Infinte Scroll*/

    var goal = 0;
    var reached = false;
    var window_height = parseInt(window.innerHeight);
    var last_scroll = 0;
    var posts_container;


    var checkScrollDist = function() {

        var posts_container = $("body");
        var goal = posts_container.offset().top + posts_container.outerHeight(true) - (window_height * 2);


        var scroll = $(window).scrollTop();
        // console.log(scroll)
        // console.log(goal)
        if (scroll > last_scroll) {
            if (scroll >= goal && reached == false) {

                reached = true;
                //get the next published article
                if ($scope.random == true) {
                    var url = APIURL + "articles/any/2016-1-1, 2016-8-1";
                } else {
                  var url = APIURL + "articles/next/" + String($scope.lastArticle._id);

                  url = $scope.leadingArticlePostType == 'video' ? url + "/type/video" : url;

                } //end else

                if ($scope.random == true || ($scope.random != true && $scope.lastArticle._id !== undefined)) {
                    $http.get(url).then(function(res) {
                        var article = res.data;
                        //set this articles order on the page
                        $scope.order++;
                        article.order = $scope.order;

                        if (typeof(article) !== 'undefined') {
                            //push the article onto the infinite articles array
                            $scope.fullArticles.push(article);

                            var posts_container = $("body");

                            $scope.lastArticle = article;
                            window_height = parseInt(window.innerHeight);
                            goal = posts_container.offset().top + posts_container.outerHeight(true) - (window_height * 2);
                        } else {
                            $("#loader").fadeOut();
                            $("#timeline-end").fadeIn();
                        }
                        reached = false;

                        //delay facebook comment box add and infinite ad injection by 1 second.
                        $timeout(function () {
                          //refresh google ads

                          if (typeof(FB) != 'undefined' && FB != null ) {
                            FB.XFBML.parse(document.getElementById("fb-comment-"+$scope.lastArticle._id));
                          }

                          if (typeof(googletag) != 'undefined' && googletag != null ) {

                            googletag.cmd.push(function() {
                              var mapping1 = googletag.sizeMapping().
                            		addSize([1024, 768], [728, 90]).
                            		addSize([980, 690], [728, 90]).
                            	  addSize([640, 480], [300, 250], [320, 50]).
                            		addSize([0, 0], [300, 250], [320, 50]).
                            	  build();

                            	var mapping2 = googletag.sizeMapping().
                            		addSize([1024, 768], [300, 250], [320, 50]).
                            		addSize([980, 690], [300, 250], [320, 50]).
                            		addSize([640, 480], [300, 250], [320, 50]).
                            		addSize([0, 0], [300, 300], [320, 50]).
                            		build();


                              //Adslot 4 declaration
                           		var slotA = googletag.defineSlot('/63052033/bla', [[728,90],[300,250], [320,50]], 'adSlotA-'+article._id).defineSizeMapping(mapping1).
                           	  setCollapseEmptyDiv(true).setTargeting('pos',['infinite']).setTargeting('page',['home']).addService(googletag.pubads());

                              //Adslot 2 declaration
                              var slotB = googletag.defineSlot('/63052033/bla', [[300,250], [320,50]], 'adSlotB-'+article._id).defineSizeMapping(mapping2).
                              setCollapseEmptyDiv(true).setTargeting('pos',['300x250_MedRecATF_102']).setTargeting('page',['home']).addService(googletag.pubads());
                       	     // Display has to be called before
                       	     // refresh and after the slot div is in the page.
                             googletag.display('adSlotA-'+article._id);
                       	     googletag.display('adSlotB-'+article._id);
                       	     googletag.pubads().refresh([slotA, slotB]);
                       	   });

                         }



                        }, 1000);

                    }); //end get url
                }
            } //end if

        } else {
            // upscroll code
        }

        last_scroll = scroll;
    }; //end checkScrollDist




//if this is NOT the homepage
if( window.location.pathname.trim() != '/'){
  $(window).scroll(checkScrollDist);
}


    /*Infinte Scroll*/
    /*Infinte Scroll*/
    /*Infinte Scroll*/

    //get articles
    $scope.loadNewArticles = function() {
        $http.get(APIURL + "articles/3").then(function(res) {
            $scope.infinite_articles = res.data;
            $scope.newArticleCount = 0;
            // console.log(res.data);
        });
    }; //end loadNewArticles

    $scope.getRelatedArticles = function(article) {
        $http.get(APIURL + "related/article.slug/3").then(function(res) {
            $scope.articles = res.data;
            // console.log(res.data);
        });
    }; //end getRelatedArticles


    $scope.bookmarkArticle = function(article) {
        if (typeof(window.localStorage !== 'undefined')) {
            //get the value of viewedArticles, parse into json object, push article id into the object
            var bookmarkedArticles = typeof(window.localStorage.getItem('bookmarkedArticles')) === "string" ? JSON.parse(window.localStorage.getItem('bookmarkedArticles')) : {};
            bookmarkedArticles.articles = typeof(bookmarkedArticles.articles) !== 'undefined' && bookmarkedArticles.articles.length > 0 ? bookmarkedArticles.articles : [];
            bookmarkedArticles.articles.push({
                id: article._id,
                slug: article.slug
            });
            window.localStorage.bookmarkedArticles = JSON.stringify(bookmarkedArticles);
            // console.log(window.localStorage.bookmarkedArticles);
        }
    };


    $scope.closeSubmissionForm = function() {
        $scope.article = null;
    }; //end getRelatedArticles

    // //format slugs automatically
    // $scope.formatSlug = function(title){
    //   return title.toLowerCase().replace(/\s+/g, "-"); //make string lowercase
    //   $scope.apply();
    // };

    // $scope.openEditArticle = function(article, event){
    //   $("#submission-form-container").fadeIn();
    //   var article_top = event.pageY + "px";
    //   if(parseInt(screen.width) <= 400){//if on mobile
    //     //just use css to open the form at the top left absolute
    //     $("#submission-form-container").css("top", $(window).scrollTop());

    //   }else{
    //     $("#submission-form-container").css("top", article_top);
    //   }//end else
    //   $scope.article = article;
    //   $scope.article.tags = article.tags.toString(); //turn tags to back into a comma delim string when editing
    //   // $scope.article.slug = $scope.formatSlug(article.title);//format the slug based on the title

    // };//end openEditArticle


      $scope.saveArticleEdit = function(article){
        // var response = ArticleService.saveArticleEdit(article);
        consolel.log(response);
      };

      $scope.createArticle = function(article){
        // var response = ArticleService.createArticle(article);
        consolel.log(response);
      };

      $scope.submitArticle = function(article){
        // var response = ArticleService.submitArticle(article);
        consolel.log(response);
      };


      $scope.publishArticle = function(article){
        // var response = ArticleService.publishArticle(article);
        consolel.log(response);
      };//end publishArticle


    //saves the article by posting it to the api save endpoint in the correct format
    $scope.createArticle = function(article) {
        //turn tags into array
        if (article.tags.length > 0) {} else {
            article.tags = typeof(article.tags) === 'undefined' ? "" : article.tags.split(/\s*,\s*/);
        }
        // console.log(article.tags);

        $http({
            method: 'POST',
            url: APIURL + 'articles',
            //IMPORTANT!!! You might think this should be set to 'multipart/form-data'
            // but this is not true because when we are sending up files the request
            // needs to include a 'boundary' parameter which identifies the boundary
            // name between parts in this multi-part request and setting the Content-type
            // manually will not set this boundary parameter. For whatever reason,
            // setting the Content-type to 'false' will force the request to automatically
            // populate the headers properly including the boundary parameter.
            headers: {
                'Content-Type': undefined
            },
            //This method will allow us to change how the data is sent up to the server
            // for which we'll need to encapsulate the model data in 'FormData'
            transformRequest: function(data) {
                var formData = new FormData();
                //need to convert our json object to a string version of json otherwise
                // the browser will do a 'toString()' on the object which will result
                // in the value '[Object object]' on the server.
                formData.append("article", angular.toJson(article));
                //now add all of the assigned files
                for (var i = 0; i < $scope.files.length; i++) {
                    //add each file to the form data and iteratively name them
                    formData.append("files", $scope.files[i]);
                }
                return formData;
            },
            //Create an object that contains the model and files which will be transformed
            // in the above transformRequest method
            data: {
                article: article,
                files: $scope.files
            }
        }).success(function(data, status, headers, config) {
            //socket.emit('article created', {article: article});
            //empty the (new) article variable
            $("#submission-form-container").fadeOut();
            $scope.article = null;
        }).error(function(data, status, headers, config) {
            alert("failed!");
        });

        //   $http.post('articles', {article: article}).then(function(response){
        // });//end post to new article
    }; //end createArticle


    //saves the comment by posting it to the api save endpoint in the correct format
    $scope.saveComment = function(article) {
        var comment = {};
        comment.body = article.newComment;
        comment.created_at = Date.now();
        comment._article = article._id;
        article.commentCount += 1;
        article.comments.unshift(comment);


        $http.post(APIURL + 'articles/comments', {
            comment: comment
        }).then(function(response) {
            console.log(response);
            return;

            //emit event to everyone except the person who posted it.
            if (response == "Unauthorized") {
                var index = article.comments.indexOf(comment)
                article.comments.splice(index, 1);
                console.log(article);
            } else if (response.data.code && response.data.code == 100) {
                //socket.emit('comment created', {comment: comment});

            } else {
                var index = article.comments.indexOf(comment)
                article.comments.splice(index, 1);
                console.log(article);
            }
            alert("success!");
            //emit event to everyone except the person who posted it.

        }); //end post to new article
        //empty the (new) comment variable
        article.newComment = null;

    }; //end saveComment


    $scope.deleteComment = function(comment) {

        var cnf = confirm("Are you sure you want to delete this comment?");

        if (cnf) {
            $http.delete(APIURL + 'articles/comments/' + article._id).then(function(respo) {
                // socket.emit('article deleted', {article: article});
                var index = $scope.fullArticles.indexOf(article);
                $scope.fullArticles.splice(index, 1);
            });
        } else {
            //do nothing
        }

    }; //end deleteArticle

    $scope.deleteArticle = function(article) {

        var cnf = confirm("Are you sure you want to delete this article?");

        if (cnf) {
            $http.delete(APIURL + 'articles/' + article._id).then(function(respo) {
                // socket.emit('article deleted', {article: article});
                var index = $scope.fullArticles.indexOf(article);
                $scope.fullArticles.splice(index, 1);
            });
        } else {
            //do nothing
        }

    }; //end deleteArticle

    $scope.searchArticles = function(search) {
        $(".loader").fadeIn();

        $http.get(APIURL + 'articles/search/' + search).then(function(response) {
            // $("#mobile-nav").slideUp();
            $(".loader").fadeOut();
            $("html, body").animate({
                scrollTop: 0
            }, "fast"); //scroll to the top of the page automatically. (500 so we scroll past the featured article)

            if (response.data.hits.length > 0) {
                $scope.searchResults = [];
                for (var i = 0; i < response.data.hits.length; i++) {
                    $scope.searchResults.push(response.data.hits[i].document);
                }
            }

            $("#search-results-end").fadeIn();

            // console.log(response.data.hits);
        }); //end post to new article
    }; //end searchArticles

    //autoformat the slugs from the title
    $('#slug').bind('keyup keypress blur', function() {
        var slug = $(this).val();
        slug = slug.toLowerCase().replace(/\s+/g, "-"); //replace spaces with dashes

        // $('#slug').val(slug);
        $scope.article.slug = slug;
    });

    $('#title').bind('keyup keypress blur', function() {
        var title = $(this).val();
        var slug = title.toLowerCase().replace(/\s+/g, "-"); //replace spaces with dashes
        // $('#slug').val(slug);
        $scope.article.slug = slug;

    });

}); //end article controller

app.controller("AuthorCtrl", function($scope, $http, $filter, $cookies, $sce) {
    $scope.similarpost = {};
    $scope.isSimilarpost = false;

});

app.controller("AddPostCtrl", function($scope,$rootScope, $http, $filter, $cookies, $sce, selectService, Slug, $timeout,apiRequest) {
    if(!$rootScope.isUserLogin()){
        window.location.replace('/login')
    }
    $scope.nowDate = new Date();
    $scope.posttype = 0;
    $scope.isSimilarpost = false;
    $scope.postTitle = '';
    $scope.postSubTitle = '';
    $scope.postDesc = '';
    $scope.excerpt = '';
    $scope.taglist = new Array();
    $scope.categorylist = new Array();
    $scope.categories = new Array();
    $scope.tags = new Array();
    $scope.files = new Array();
    $scope.recentPost=[];
    $scope.totalPost=0;
    $scope.typeOfPost = ['opinion', 'listicle', 'video', 'news', 'other'];
    $scope.videourl='';
    $scope.runTimer = function() {
        $scope.nowDate = new Date();
        mytimer = $timeout($scope.runTimer, 1000);
    }
    var mytimer = $timeout($scope.runTimer, 1000);
    apiRequest.getUserInfo().success(function(data, status, headers, config) {
        console.log(data)
        if(data.success==false){
            $rootScope.loader=true;
            window.location.assign('/login');
        }
        $rootScope.user=data.user;
            /*----------Reading Total UserPost with N latest Article------*/
            apiRequest.getRequest('articles/authorsWillAllCount/',data.user.username+'/10')
            .success(function (data,status,headers,config) {
                console.log(data)
                 $scope.recentPost=data.articles;
                 $scope.totalPost=data.totalArticle;
             })
            .error(function(data, status, headers, config) {
                console.log(data)
                //alert(data.msg)
            });

    }).error(function(data, status, headers, config) {
        console.log(data)
        //alert(data.msg)
    });
    $scope.isValidPost=false;
    $scope.isValidDraft=false;
    $scope.validatePost=function () {

        if($scope.posttype==0 || $scope.posttype==3)
            $scope.isValidDraft= ($scope.postTitle!='' && $scope.postSubTitle!='' && $scope.excerpt!='' ) ? true : false;

        else if($scope.posttype==1)
            $scope.isValidDraft= ($scope.postTitle!='' && $scope.postSubTitle!=''  ) ? true : false;

        else if($scope.posttype==2){
            $scope.isValidDraft= ($scope.postTitle!='' && $scope.postSubTitle!='' && $scope.excerpt!='' && $scope.videourl!=='' ) ? true : false;
        }
     }
    $scope.$watch('[postTitle,posttype,postSubTitle,postDesc,excerpt,videourl]',function (oldvalue, newvalue) {
            $scope.validatePost();
      })
      $scope.$watchCollection('[categorylist,isValidDraft]',function () {
          if($scope.isValidDraft){
            $scope.postDesc=$('#postBody' + $scope.posttype + '.text-editor').froalaEditor('html.get');
          }

          $scope.isValidPost= ($scope.isValidDraft && $scope.categorylist.length >0 && $scope.postDesc!='') ? true : false;
       })

    selectService.getCategory().success(function(res) {
        //console.log(res)
        $scope.categories = res;
    });
    selectService.getTags().success(function(res) {
        // console.log(res)
        $scope.tags = res;
    });

    /*Get Current Users Aricles*/
    /*Save post as draft*/
    $scope.saveAsDraft = function() {
        alert('for draft')
    };
    $scope.$watch('posttype', function(newvalue, oldvalue) {
        //alert('newvalue'+newvalue+'   oldvalue'+oldvalue);
        if (newvalue !== oldvalue) {
            var oldEdior = $('#postBody' + oldvalue + '.text-editor').froalaEditor('html.get');
            var newEdior = $('#postBody' + newvalue + '.text-editor').froalaEditor('html.get');
            // alert('onl length'+oldEdior.length+'    new'+newEdior.length);
            if (oldEdior.length > newEdior.length) {
                $('#postBody' + newvalue + '.text-editor').froalaEditor('html.set', oldEdior);
            }

        }
    });
    //saves the article by posting it to the api save endpoint in the correct format
    $scope.createArticle = function(article) {
        //turn tags into array
        // if (article.tags.length > 0) {
        // } else {
        //     article.tags = typeof(article.tags) === 'undefined' ? "" : article.tags.split(/\s*,\s*/);
        // }
        // console.log(article.tags);

        $http({
            method: 'POST',
            url: APIURL + 'articles',
            //IMPORTANT!!! You might think this should be set to 'multipart/form-data'
            // but this is not true because when we are sending up files the request
            // needs to include a 'boundary' parameter which identifies the boundary
            // name between parts in this multi-part request and setting the Content-type
            // manually will not set this boundary parameter. For whatever reason,
            // setting the Content-type to 'false' will force the request to automatically
            // populate the headers properly including the boundary parameter.
            headers: {
                'Content-Type': undefined,
                'token': $rootScope.accessToken
            },
            //This method will allow us to change how the data is sent up to the server
            // for which we'll need to encapsulate the model data in 'FormData'
            transformRequest: function(data) {
                var formData = new FormData();
                //need to convert our json object to a string version of json otherwise
                // the browser will do a 'toString()' on the object which will result
                // in the value '[Object object]' on the server.
                formData.append("article", angular.toJson(article));
                //now add all of the assigned files
                for (var i = 0; i < $scope.files.length; i++) {
                    //add each file to the form data and iteratively name them
                    formData.append("files", $scope.files[i]);
                }
                return formData;
            },
            //Create an object that contains the model and files which will be transformed
            // in the above transformRequest method
            data: {
                "article": article,
                "files": $scope.files
            }
        }).success(function(data, status, headers, config) {
            //socket.emit('article created', {article: article});
            //empty the (new) article variable

            alert('Post save successfully');
            location.reload();
            //location.reload();
        }).error(function(data, status, headers, config) {
            alert("failed!");
        });
        return true;

        //   $http.post('articles', {article: article}).then(function(response){
        // });//end post to new article
    }; //end createArticle


    /*Save post for submission
    asAs=true so save as submited
    asAs=false so save as draft
    */
    $scope.postForSubmit = function(asAs) {
        var listicle = new Array();
        if ($scope.typeOfPost[$scope.posttype] == 'listicle') {
            $('.listicle-content').each(function() {
                //var items = new Array();
               // items['title'] = $(this).children('div').children('.listicle-title').val();
               // items['body'] = $(this).children('.listicle-body').froalaEditor('html.get');
               var items={"title":$(this).children('div').children('.listicle-title').val(),
                "body": $(this).children('.listicle-body').froalaEditor('html.get')}
                listicle.push(items);
            })
            $scope.excerpt='';
        }
        else if ($scope.typeOfPost[$scope.posttype] !== 'video') {
            $scope.videourl='';
        }
        var sc='draft';
        if(asAs){
            sc='submited';
        }
        var articleObj = {
            type: $scope.typeOfPost[$scope.posttype],
            postType: $scope.typeOfPost[$scope.posttype],
            videourl:$scope.videourl,
            _tags: $scope.taglist,
            _categories: $scope.categorylist,
            slug: Slug.slugify($scope.postTitle),
            title: $scope.postTitle,
            subTitle: $scope.postSubTitle,
            commentStatus: false,
            published: false,
            post_status: sc,
            wp_featuredImage:'null',
//            listicle: JSON.stringify(listicle),
            listicle: listicle,
            // _author: "577371048b70ef5d3813447d",
            body: $('#postBody' + $scope.posttype + '.text-editor').froalaEditor('html.get'),
            excerpt: $scope.excerpt
        };
        console.log(articleObj);
        $scope.createArticle(articleObj);
    }

});


app.controller("AuthCtrl", function($scope, $http, $filter, $cookies, $sce, $rootScope, $location) {

    $scope.user = null;
    if($cookies.get('token') !== undefined){
        $rootScope.loader = true;
        window.location.replace('/');
    }
    $scope.authError = false;
    $scope.verificationMessage = false;
    $scope.verificationStatus = null;
    $scope.erroMsg = '';
    $scope.successMsg = '';
    $scope.requestResult = '';
    $scope.requestMsg = '';
    $scope.loginFormValid = false;
    $scope.registerFormValid = false;
    $scope.requestSent = false;
    $scope.user = {
        email: "",
        username:"",
        password: "",
        passwordAgain:''
    };
    $scope.forgottenEmail = "";

    $scope.validateLoginForm = function () {
        $scope.loginFormValid = (   (typeof $scope.user.email !== undefined && $scope.user.email != '' && $scope.loginForm.email.$valid) &&
                                    (typeof $scope.user.password !== undefined && $scope.user.password != '')) ? true : false;
    };
    $scope.$watch('[user.email,user.password]',function () {
        $scope.validateLoginForm();
        //$scope.erroMsg='';
        //$scope.successMsg='';

     },true);

    $scope.validateRegisterForm = function () {
         $scope.registerFormValid = ((typeof $scope.user.username !== undefined && $scope.user.username != '' && $scope.registerForm.email.$valid) &&
             (typeof $scope.user.email !== undefined && $scope.user.email != '') &&
             (typeof $scope.user.password !== undefined && $scope.user.password != '') &&
             ($scope.user.password === $scope.user.passwordAgain ) ) ? true : false;

    };

    $scope.$watch('[user.username,user.email,user.password,user.passwordAgain]',function () {
        //$scope.erroMsg='';
        //$scope.successMsg='';
        $scope.validateRegisterForm();
        //console.log($scope.user)
     },true);

    $scope.sendReset = function(email) {
        console.log(email);
        $http.post(APIURL+"send-reset-link", {
            email: email
        }).success(function(data, status, headers, config) {
            if (data.success){
                $scope.successMsg = data.msg;
            }else{
                $scope.errorMsg = data.msg;
            }
        }).error(function(data, status, headers, config) {
            $scope.errorMsg = data.msg;
        });
    };

    $scope.sendNewPassword  = function(password, token) {
        $http.post(APIURL+"change-password", {
            password: password,
            token: token
        }).success(function(data, status, headers, config) {
            console.log(data);
            if (data.success){
                $scope.successMsg = data.msg;
            }else{
                $scope.errorMsg = data.msg;
            }
        }).error(function(data, status, headers, config) {
            $scope.errorMsg = data.msg;
        });
    };

    //authentication

    $scope.signUp = function(user) {
        //user.username=user.email;
        console.log(user);
        $http.post(APIURL+"auth/signup", {
            user: user
        }).success(function(data, status, headers, config) {
            //socket.emit('article created', {article: article});
            //empty the (new) article variable
            console.log(data);
            if (data.success){
                $scope.requestResult = 'success';
                $scope.requestMsg = 'Register successfully. Please Check Your Mail';
                $scope.user = '';
                //alert('User Register successfully');
            }else{
               $scope.requestResult = 'error';
               $scope.requestMsg = data.msg
            }
            return false;
        }).error(function(data, status, headers, config) {
            alert("failed!");
        });
    };

    $scope.signIn = function(user) {
        console.log("user: " + user.email);

        $http.post(APIURL+"auth/authenticate", {
            user: user
        }).success(function(data, status, headers, config) {
            //socket.emit('article created', {article: article});
            //empty the (new) article variable
            console.log(data);
              if(data.success){
               $scope.authError = false;
               // $cookies.put('token',data.token);
                window.location.assign('/');
            }
            else{
               $scope.authError = true;
            }
            return true;

        }).error(function(data, status, headers, config) {
           $scope.authError=true;
            return false;
        });
    };

    //end authentication

}); //end auth controller

app.service('selectService', function($http) {
    this.getCategory = function() {
        return $http.get(APIURL + 'category/100');
    };

    this.getTags = function() {
        return $http.get(APIURL + 'tags/100');
    }
});

app.service('apiRequest', function($http,$rootScope,$cookies) {
    this.getRequest = function(url,param) {
        return $http.get(APIURL +url+param);
    };
    this.getUserInfo=function () {
        return $http({
            method: 'POST',
            url: APIURL + 'getUserInfo',
            headers: {
                'Content-Type': undefined,
                'token': $cookies.get('token')
            },
            data: {}
        })
     };
});


$("#main-hider").fadeOut("slow");

//Check to see if the window is top if not then display button
$(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
        $('.scroll_top').fadeIn();
    } else {
        $('.scroll_top').fadeOut();
    }
});

//Click event to scroll to top
$('.scroll_top').click(function() {
    $('html, body').animate({
        scrollTop: 0
    }, 800);
    return false;
});





var scrollIndex = 0; //(record of articles scrolled past) Which one we're 'on'

$(window).scroll(function() {

    //Check to see if the window is top if not then display button
      if ($(this).scrollTop() > 100) {
          $('.scroll_top').fadeIn();
      } else {
          $('.scroll_top').fadeOut();
      }
    //end to top button



    //get all the scroll markers
    var scrollMarkers = $(".scrollMarker");
    //get the distance the user has scrolled from the top of the page
    var fromTop = $(window).scrollTop();
    //for each scroll marker on the page
    for (var i = 0; i < scrollMarkers.length; i++) {
      //get it's distance from the top of the page
      var y = $(scrollMarkers[i]).offset().top;

      //if we've scrolled past that distance
      //and if the scrollindex (record of articles scrolled past) matches this iteration (this scrollMarker)
        if ( scrollIndex == i && fromTop > Math.round(y) ){
          scrollIndex++;// on to the next one

          // console.log("------------------------");
          // console.log("Marker " + i );
          // console.log("y position: "+ y );
          // console.log("scrolled from Top: ");
          // console.log(fromTop);
          // console.log("------------------------");
          //get the url and title from the marker
          var url = $(scrollMarkers[i]).attr('data-url');
          var title = $(scrollMarkers[i]).attr('data-title');

          //push it's article to history state
          document.title = title;
          window.history.pushState("", "Blavity - " + title, url);

          // console.log("currentPage: " + title);
          // console.log("it's url: " + url);


        }//end if

    }//end for

});
