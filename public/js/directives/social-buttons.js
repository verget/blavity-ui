var template = 'components/social-buttons.html';

angular.module('StaticRealtimeBlog')
    .directive('socialButtons', socialButtons)
    .name;

function socialButtons() {
    return {
        restrict: 'E',
        scope: {  },
        templateUrl: template,
        link: function(scope, element) {
            scope.shareFb = function(){
                FB.ui({
                    method: 'feed',
                    name: element.attr('data-title'),
                    link: 'http://preview.blavity.com/' + element.attr('data-slug'),
                    picture: element.attr('data-picture'),
                    //caption: fullArticle.excerpt,
                    description: 'Blavity is an awesome site, with more stuff like this',
                    // message: 'Message you want to show'
                });
            };
            scope.shareTwitter = function(){
                $location.path("https://twitter.com/intent/tweet?text="+element.attr('data-title')+"&url=http://preview.blavity.com/"+element.attr('data-slug')+"&via=blavity");
            };
            scope.shareMail = function(){
                $location.path("mailto:?Subject=Check%20Out%20This%20Blavity%20Post%20-%20"+element.attr('data-title')+"&Body=http://preview.blavity.com/"+element.attr('data-slug')+"");
            };
            scope.shareIOS = function(){
                $location.path("sms:&body=Check out this Blavity post, http://preview.blavity.com/"+element.attr('data-slug')+" - "+element.attr('data-title'));
            };
            scope.shareAndroid = function(){
                $location.path("sms:?body=Check out this Blavity post, http://preview.blavity.com/"+element.attr('data-slug')+" - "+element.attr('data-title'));
            };
        }
    };
};