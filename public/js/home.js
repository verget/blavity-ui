
    //var app = angular.module("StaticRealtimeBlog", ['ngAnimate', 'ngCookies','angular-loading-bar']);
    // so we can display html as html and not text... you know what i mean. unescaped
'use strict';
var APIURL = "http://api.blavity.com/v1/";
//var APIURL = "http://localhost:3100/v1/";
var app = angular.module("StaticRealtimeBlog", ['ngAnimate', 'ngCookies', 'ngSanitize', 'angulike', 'angular-loading-bar', 'ui.select2', 'slugifier']).run([
    '$rootScope','$cookies','apiRequest',
    function($rootScope,$cookies,apiRequest) {
        $rootScope.facebookAppId = '567482886746994'; // set your facebook app id here
        $rootScope.user='';
        $rootScope.accessToken=$cookies.get('token');
        console.log($rootScope.accessToken)
        $rootScope.isLogin=false;
        $rootScope.isUserLogin=function () {
                $rootScope.accessToken=$cookies.get('token');
                if($rootScope.accessToken!==undefined || $rootScope.accessToken!==undefined ){
                    $rootScope.isLogin=true;
                    return true;
                }
                else{
                    $rootScope.isLogin=false;
                    return false;
                }
          }
          $rootScope.isLogin= $rootScope.isUserLogin();
          $rootScope.logout=function(){
              $cookies.remove("userInfo");
              location.reload();
          }
          $rootScope.getUserInfo=function () {  
              $rootScope.isLogin=false;
              if($cookies.get('token')!==undefined  || $rootScope.accessToken!==undefined){
                    apiRequest.getUserInfo().success(function(data, status, headers, config) {
                        console.log(data)
                        $rootScope.user=data.user;
                        $rootScope.isLogin=true;                
                    }).error(function(data, status, headers, config) {
                        console.log(data);
                        $rootScope.isLogin=false;
                        alert(data.msg)
                    });
              }
              else{
                $rootScope.isLogin=false;
              }

          }
          
    }
]);
app.controller("headerCTL", function($scope, $http, $filter, $cookies, $sce,$rootScope) {
    if($rootScope.isLogin){
    $rootScope.getUserInfo();

    }
})
    app.service('apiRequest', function($http,$rootScope,$cookies) {
        this.getRequest = function(url,param) {
            return $http.get(APIURL +url+param);
        };
        this.getUserInfo=function () {
            return $http({
                method: 'POST',
                url: APIURL + 'getUserInfo',
                headers: {
                    'Content-Type': undefined,
                    'token': $cookies.get('token')
                },
                data: {}
            })
        };
    });

    app.filter('unsafe', function($sce) {
        return function(val) {
            return $sce.trustAsHtml(val);
        };
    });

    app.filter('htmlToPlaintext', function() {
      return function(text) {
        return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
      };
    });

    app.filter('truncatePostContent', function() {
      return function(content, maxLength) {
        var content = content ? String(content).replace(/<[^>]+>/gm, '') : '';//remove html tags from string
        //trim the string to the maximum length
        var trimmedString = content.substr(0, maxLength);
        //re-trim if we are in the middle of a word
        return trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")));
      };
    });


    app.filter('hash', function() {
        return function(val) {
          if(typeof val === 'undefined' || val.trim() === '')
            return '';

          return md5(val);
        };
    });

    //this adds the contenteditable directive so that we can use in-place editing with angular's model binding
    app.directive("contenteditable", function() {
      return {
        restrict: "A",
        require: "?ngModel",
        link: function(scope, element, attrs, ngModel) {
           if (!ngModel) {return;} // do nothing if no ng-model

            ngModel.$render = function() {
              element.html(ngModel.$viewValue);
            };

            element.on('blur keyup change', function() {
              scope.$evalAsync(read);
            });
            ngModel.$render();

            function read() {
              var html = element.html();
              // When we clear the content editable the browser leaves a <br> behind
              // If strip-br attribute is provided then we strip this out
              if ( attrs.stripBr && html == '<br>' ) {
                html = '';
              }
              ngModel.$setViewValue(html);
            }
        }
      };
    });
    app.directive('checkImage', function($http) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            attrs.$observe('src', function(src) {
                if(src=='')
                  element.attr('src', 'http://placehold.it/240x240');
                $http.get(src).success(function(){
                    //alert('image exist');
                }).error(function(){
                  //  alert('image not exist');

                  if(src=='')
                    element.attr('src', 'http://placehold.it/240x240'); // set default image
                });
            });
        }
    };
});


// app.directive('isInView', function() {
//   return {
//     restrict: 'AE',
//     replace: false,
//     link: function(scope, elem, attrs) {

//       $document.bind('scroll', function () {
//           // scope.$apply(attrs.scrolly);
//           alert("scrolled");
//       });



//       elem.bind('click', function() {
//         // if(elem.offset().top <= 100){
//           elem.css('background-color', 'blue');
//           scope.$apply(function() {
//             scope.color = "grey";
//           });
//         // }

//       });
//       elem.bind('mouseover', function() {
//         elem.css('cursor', 'pointer');
//       });
//     }
//   };
// });





app.directive('fileUpload', function () {
    return {
        scope: true,        //create a new scope
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                //iterate files since 'multiple' may be specified on the element
                for (var i = 0;i<files.length;i++) {
                    //emit event upward
                    scope.$emit("fileSelected", { file: files[i] });
                }
            });
        }
    };
});



app.config(["$httpProvider", function ($httpProvider) {
  $httpProvider.interceptors.push(function($q, $location, $cookies) {
    return { response: function(response) {
      // do something on success

        if(response.data){
          if(response.data.token){
            if(response.data.success == true){
            // if(response.data.success == true){
              $cookies.put('token', response.data.token);
              console.log($cookies.getAll());
              // window.localStorage.token = response.data.token;
            }else{
              // console.log(response.data.msg);
              // alert("Sorry, sign in failed for some reason!");
              // window.location = "http://jordancryingface.com";
            }
          }//end if response has token
        }//end if response has data

      return response;
      },
      responseError: function(response) {
        if (response.status === 401)
          alert("401!");

        if (response.status === 403)
          alert("403!");

        return $q.reject(response);
      }

    };

    return {
     'request': function(config) {
          config.headers['token'] = $cookies.get('token');
          return config;
      }
    };

  });
}]);



    app.directive('dynFbCommentBox', function () {
        function createHTML(href, numposts, colorscheme, width) {
            return '<div class="fb-comments" ' +
                           'data-href="' + href + '" ' +
                           'data-numposts="' + numposts + '" ' +
                           'data-colorsheme="' + colorscheme + '" ' +
                           'data-width="' + width + '">' +
                   '</div>';
        }


        return {
            restrict: 'A',
            scope: {},
            link: function postLink(scope, elem, attrs) {
                attrs.$observe('pageHref', function (newValue) {
                    var href        = newValue;
                    var numposts    = attrs.numposts    || 5;
                    var colorscheme = attrs.colorscheme || 'light';
                    var width = attrs.width || '100%';
                    elem.html(createHTML(href, numposts, colorscheme, width));
                    FB.XFBML.parse(elem[0]);
                });
            }
        };
    });

    app.directive('dynFbShares', function () {
        function createHTML(href) {
            return '<i class="fa fa-facebook data-href="http://paistre.com/' + href + '" fb-share-button"></i>';
        }

        return {
            restrict: 'A',
            scope: {},
            link: function postLink(scope, elem, attrs) {
                attrs.$observe('pageHref', function (newValue) {
                    var href        = newValue;
                    elem.html(createHTML(href));
                    FB.XFBML.parse(elem[0]);
                });
            }
        };
    });

    app.directive('dynFbLikes', function () {
        function createHTML(href) {
            return '<i class="fa fa-thumbs-up fb-like-button" data-href="http://paistre.com/' + href + '" data-layout="standard" data-action="like"></i>';
        }

        return {
            restrict: 'A',
            scope: {},
            link: function postLink(scope, elem, attrs) {
                attrs.$observe('pageHref', function (newValue) {
                    var href        = newValue;
                    elem.html(createHTML(href));
                    FB.XFBML.parse(elem[0]);
                });
            }
        };
    });

    app.controller("ArticleCtrl", function ($scope, $http, $filter, $cookies) {
    //an array of files selected
    $scope.files = [];

    //listen for the file selected event
    $scope.$on("fileSelected", function (event, args) {
        $scope.$apply(function () {
            //add the file object to the scope's files collection
            $scope.files.push(args.file);
        });
    });

      $scope.featuredArticle = null;
      $scope.lastArticle = {};
      $scope.infinite_articles = [];
      $scope.more_articles = [];
      $scope.fullArticles = [];
      $scope.editMode = false;
      $scope.newArticleCount = 0;
      $scope.searchResults = [];
      $scope.user = null;
      $scope.user = {email:"test@test.com", password: "test"};
      $scope.page_position = 0;
      $scope.npArticlesLoaded = [];
      $scope.theAuthor = null;
      $scope.random = false;
      $scope.order = 0;
      $scope.offset = 15;

// //sockets - realtime updates
//       //open up a socket.io for emitting and listening to events through sockets
//       // var socket = io('http://45.55.30.92:3000');
//       //when the acrticle created event is detected
//       socket.on('article created', function(resp){
//         // $scope.$apply();//so angular will refresh the view and add our flash message
//       });//end on article published
//
//       socket.on('article published', function(resp){
//         console.log(resp.article._id);
//         $scope.npArticlesLoaded = [];
//         $scope.$apply();//so angular will refresh the view and add our flash message
//         //get the article that triggered the event using the api by it's slug
//         // $http.get('http://api.blavity.com/v1/' + resp.article.slug ).then(function(rsp){
//         //   $scope.infinite_articles.push(rsp.data);
//         //   console.log(rsp.data);
//         // });//end get article by slug
//       });//end on article published
// //end sockets


//authentication


      $scope.signUp = function(user){
        $http.post("http://paistre.com/signup", {user:user}, function( res ){
          console.log(res.data);
        });
      };

      $scope.signIn = function(user){
        console.log("user: "+ user.email);

        $http.post("http://paistre.com/authenticate", {user:user}, function( res ){

        });
      };

//end authentication

//page initialization
      //get a random article to be featured on the 'homepage'
      // $http.get("http://api.blavity.com/v1/articles/1").then(function( res ){
      //   $scope.featuredArticle = res.data[0];
      //   // console.log(res.data[0]);
      // });

      $scope.getRandomArticle = function(range){
        // range ex. '2016-1-1, 2016-5-1'
        var range = typeof(range) === "undefined" ? "" : range;
        $http.get(APIURL+"articles/any/" + range).then(function( res ){
          console.log(res.data.title);
          return res.data;
        });
      };

// setInterval($scope.getRandomArticle, 5000);

    $scope.loadMoreArticles = function(){

      $http.get(APIURL+"articles/5/" + $scope.offset).then(function( res ){
        //increase the offset

        $scope.offset += 5;

        //order each of the five articles
        for (var i = res.data.length - 1; i >= 0; i--) {
          res.data[i].order = $scope.order;
          $scope.order++;
          //push each of the articles onto the more articles array
          $scope.more_articles.push(res.data[i]);
        };

        $scope.lastArticle = $scope.more_articles[$scope.more_articles.length - 1];
      });
    };

//when the window loads, ajax in a set of articles
$scope.loadMoreArticles();
//end page initialization

$scope.loadComments = function(article){

  $http.get(APIURL+"articles/comments/" + article._id + "/20").then(function( res ){
    console.log(res.data);
    article.comments = (res.data);
  });
};

for (var i = $scope.fullArticles.length - 1; i >= 0; i--) {
  $scope.loadComments($scope.fullArticles[i]);
};


$scope.likeArticle = function(article){
  //make likes an empty array if one doesn't already exist on the article
  article.likes = typeof(article.likes === 'undefined') ? [] : article.likes;
  article.likes.push("user1234");
  //todo://push the actual user id
  return;

};

/*Infinte Scroll*/
/*Infinte Scroll*/
/*Infinte Scroll*/

    // var goal = 0;
    // var reached = false;
    // var window_height = parseInt(window.innerHeight);
    // var last_scroll = 0;
    // var posts_container;
    //
    //
    // checkScrollDist = function() {
    //
    //   var posts_container = $( "body" );
    //   var goal = posts_container.offset().top + posts_container.outerHeight(true) - ( window_height * 2 );
    //
    //
    //   var scroll = $(window).scrollTop();
    //   if (scroll > last_scroll){
    //     if(scroll >= goal && reached == false){
    //
    //       reached = true;
    //       //get the next published article
    //       if($scope.random == true){
    //         var url = "http://api.blavity.com/v1/articles/any/2016-1-1, 2016-5-1";
    //       }else{
    //         var url = "http://api.blavity.com/v1/articles/next/" + String($scope.lastArticle._id);
    //       }//end else
    //
    //       $http.get( url ).then(function( res ){
    //         article = res.data;
    //         //set this articles order on the page
    //         $scope.order++;
    //         article.order = $scope.order;
    //
    //         if(typeof(article) !== 'undefined'){
    //           //push the article onto the infinite articles array
    //           $scope.infinite_articles.push(article);
    //
    //           var posts_container = $( "body" );
    //
    //           $scope.lastArticle = article;
    //           window_height = parseInt(window.innerHeight);
    //           goal = posts_container.offset().top + posts_container.outerHeight(true) - ( window_height * 2 );
    //         }else{
    //           $("#loader").fadeOut();
    //           $("#timeline-end").fadeIn();
    //         }
    //         reached = false;
    //         // FB.XFBML.parse();
    //       });//end get url
    //
    //     }//end if
    //
    //   } else {
    //      // upscroll code
    //   }
    //
    //   last_scroll = scroll;
    // };//end checkScrollDist
    //
    //
    // $(window).scroll(checkScrollDist);

/*Infinte Scroll*/
/*Infinte Scroll*/
/*Infinte Scroll*/

//get articles
      $scope.loadNewArticles = function(){
        $http.get(APIURL+"articles/3").then(function( res ){
          $scope.infinite_articles = res.data;
          $scope.newArticleCount = 0;
          // console.log(res.data);
        });
      };//end loadNewArticles

      $scope.getRelatedArticles = function(article){
        $http.get(APIURL+"related/article.slug/3").then(function( res ){
          $scope.articles = res.data;
          // console.log(res.data);
        });
      };//end getRelatedArticles


      $scope.bookmarkArticle = function(article){
        if(typeof(window.localStorage !== 'undefined')){
          //get the value of viewedArticles, parse into json object, push article id into the object
          var bookmarkedArticles = typeof(window.localStorage.getItem('bookmarkedArticles')) === "string" ? JSON.parse(window.localStorage.getItem('bookmarkedArticles')) : {};
          bookmarkedArticles.articles = typeof(bookmarkedArticles.articles) !== 'undefined' && bookmarkedArticles.articles.length > 0 ? bookmarkedArticles.articles : [];
          bookmarkedArticles.articles.push( { id : article._id, slug : article.slug});
          window.localStorage.bookmarkedArticles = JSON.stringify(bookmarkedArticles);
          console.log(window.localStorage.bookmarkedArticles);
        }
      };




      $scope.closeSubmissionForm = function(){
        $scope.article = null;
      };//end getRelatedArticles

      // //format slugs automatically
      // $scope.formatSlug = function(title){
      //   return title.toLowerCase().replace(/\s+/g, "-"); //make string lowercase
      //   $scope.apply();
      // };

      // $scope.openEditArticle = function(article, event){
      //   $("#submission-form-container").fadeIn();
      //   var article_top = event.pageY + "px";
      //   if(parseInt(screen.width) <= 400){//if on mobile
      //     //just use css to open the form at the top left absolute
      //     $("#submission-form-container").css("top", $(window).scrollTop());

      //   }else{
      //     $("#submission-form-container").css("top", article_top);
      //   }//end else
      //   $scope.article = article;
      //   $scope.article.tags = article.tags.toString(); //turn tags to back into a comma delim string when editing
      //   // $scope.article.slug = $scope.formatSlug(article.title);//format the slug based on the title

      // };//end openEditArticle

      $scope.toggleEditMode = function(){
        $scope.editMode = !$scope.editMode;
        $("#mobile-nav").slideUp();
        // CKEDITOR.inlineAll();
        $('body').contents().find("*[contenteditable]").each(function() {
          var element = $(this);
          if($scope.editMode === false){
            element.attr("contenteditable", "false");
            element.removeClass("editable");
          }else{
            element.attr("contenteditable", "true");
            element.addClass("editable");
          }
        });


        $('*[contenteditable]').each(function(){
            $(this).html($(this).html().replace(/&nbsp;/gi,'').replace(/&amp;/gi,'').replace(/&lt;/gi,'').replace(/&gt;/gi,'').replace(/&quot;/gi,'').replace(/&copy;/gi,''));

        });

      }; //end toggleEditMode

      $scope.saveArticleEdit = function(article){
        //turn tags into array
        console.log(article.tags);
        if(article.tags.length > 0){
        }else if(article.tags.length > 2){
          alert("You can only add up to 3 tags!");
          return;
        }else{
          article.tags = typeof(article.tags) === 'undefined' ? "" : article.tags.split(/\s*,\s*/);
        }

        $http.put(APIURL+'articles/', {article: article} ).then(function(respo){
          // socket.emit('article edited', {article: article});
          $scope.editMode = false;
          //if you want to update the main list everytime an article is edited. (live editing)
          //but it will cause you to lose your place in infiite scroll since that's actually a different list
          // $http.get("http://api.blavity.com/v1/articles/10").then(function( res ){
          //   $scope.articles = res.data;
          //   console.log(res.data);
            $("#submission-form-container").fadeOut();
            $scope.article = null;
            $('body').contents().find("*[contenteditable='true']").each(function() {
              $(this).attr("contenteditable", "false");
                $(this).removeClass("editable");
            });
          // });//end post
        });
      };//end saveArticleEdit


      $scope.publishArticle = function(article){
        //toggle the article's pusblished state
        article.published = !article.published;
        //then just send as an update on this article
        $http.put(APIURL+'articles/', {article: article} ).then(function(response){

          var article = response.data;

          $scope.npArticlesLoaded.push(article._id);

          $scope.editMode = false;

          if(article.published == true){
            socket.emit('article published', {article: article});
          }//end if article is published

        });
      };//end publishArticle


      //saves the article by posting it to the api save endpoint in the correct format
      $scope.createArticle = function(article){
        //turn tags into array
        if(article.tags.length > 0){
        }else{
          article.tags = typeof(article.tags) === 'undefined' ? "" : article.tags.split(/\s*,\s*/);
        }
        // console.log(article.tags);

        $http({
            method: 'POST',
            url: APIURL+'articles',
            //IMPORTANT!!! You might think this should be set to 'multipart/form-data'
            // but this is not true because when we are sending up files the request
            // needs to include a 'boundary' parameter which identifies the boundary
            // name between parts in this multi-part request and setting the Content-type
            // manually will not set this boundary parameter. For whatever reason,
            // setting the Content-type to 'false' will force the request to automatically
            // populate the headers properly including the boundary parameter.
            headers: { 'Content-Type': undefined },
            //This method will allow us to change how the data is sent up to the server
            // for which we'll need to encapsulate the model data in 'FormData'
            transformRequest: function (data) {
                var formData = new FormData();
                //need to convert our json object to a string version of json otherwise
                // the browser will do a 'toString()' on the object which will result
                // in the value '[Object object]' on the server.
                formData.append("article", angular.toJson(article));
                //now add all of the assigned files
                for (var i = 0; i < $scope.files.length; i++) {
                    //add each file to the form data and iteratively name them
                    formData.append("files", $scope.files[i]);
                }
                return formData;
            },
            //Create an object that contains the model and files which will be transformed
            // in the above transformRequest method
            data: { article: article, files: $scope.files }
        }).
        success(function (data, status, headers, config) {
            alert("success!");
            //emit event to everyone except the person who posted it.
            socket.emit('article created', {article: article});
            //empty the (new) article variable
            $("#submission-form-container").fadeOut();
            $scope.article = null;
        }).
        error(function (data, status, headers, config) {
            alert("failed!");
        });

      //   $http.post('http://api.blavity.com/v1/articles', {article: article}).then(function(response){
    		// });//end post to new article
      };//end createArticle


      //saves the comment by posting it to the api save endpoint in the correct format
      $scope.saveComment = function(article){
        var comment = {};
        comment.body = article.newComment;
        comment.created_at = Date.now();
        comment._article = article._id;
        article.commentCount += 1;
        article.comments.unshift(comment);


        $http.post(APIURL+'articles/comments', {comment: comment}).then(function(response){
          console.log(response);
          return;

            //emit event to everyone except the person who posted it.
            if(response == "Unauthorized"){
              var index = article.comments.indexOf(comment)
              article.comments.splice(index, 1);
              console.log(article);
            }
            else if(response.data.code && response.data.code == 100){
              socket.emit('comment created', {comment: comment});

            }else{
              var index = article.comments.indexOf(comment)
              article.comments.splice(index, 1);
              console.log(article);
            }

        });//end post to new article
        //empty the (new) comment variable
        article.newComment = null;

      };//end saveComment


      $scope.deleteComment = function(comment){

        var cnf = confirm("Are you sure you want to delete this comment?");

        if(cnf){
          $http.delete(APIURL+'articles/comments/' + article._id ).then(function(respo){
            // socket.emit('article deleted', {article: article});
            var index = $scope.infinite_articles.indexOf(article);
            $scope.infinite_articles.splice(index, 1);
          });
        }else{
          //do nothing
        }

      };//end deleteArticle

      $scope.deleteArticle = function(article){

        var cnf = confirm("Are you sure you want to delete this article?");

        if(cnf){
          $http.delete(APIURL+'articles/' + article._id ).then(function(respo){
            // socket.emit('article deleted', {article: article});
            var index = $scope.infinite_articles.indexOf(article);
            $scope.infinite_articles.splice(index, 1);
          });
        }else{
          //do nothing
        }

      };//end deleteArticle

      $scope.searchArticles = function(search){
        $http.get(APIURL+'articles/search/' + search).then(function(response){
          $("#mobile-nav").slideUp();
          $("#loader").fadeOut();
          $("html, body").animate({ scrollTop: 0 }, "fast");//scroll to the top of the page automatically. (500 so we scroll past the featured article)

          if(response.data.hits.length > 0){
            $scope.infinite_articles = [];
            for (var i = 0; i < response.data.hits.length; i++) {
              $scope.infinite_articles.push(response.data.hits[i].document);
            }
          }

          $("#search-results-end").fadeIn();

          // console.log(response.data.hits);
        });//end post to new article
      };//end searchArticles




      //autoformat the slugs from the title
      $('#slug').bind('keyup keypress blur', function() {
          var slug = $(this).val();
          slug = slug.toLowerCase().replace(/\s+/g, "-"); //replace spaces with dashes

          // $('#slug').val(slug);
          $scope.article.slug = slug;
      });

      $('#title').bind('keyup keypress blur', function() {
          var title = $(this).val();
          slug = title.toLowerCase().replace(/\s+/g, "-"); //replace spaces with dashes
          // $('#slug').val(slug);
          $scope.article.slug = slug;

      });

    });//end article controller


$("#main-hider").fadeOut("slow");


//Check to see if the window is top if not then display button
$(window).scroll(function(){
	if ($(this).scrollTop() > 100) {
		$('.scroll_top').fadeIn();
	} else {
		$('.scroll_top').fadeOut();
	}
});

//Click event to scroll to top
$('.scroll_top').click(function(){
	$('html, body').animate({scrollTop : 0},800);
	return false;
});
