
angular.module('StaticRealtimeBlog')
 
.controller('LoginCtrl', function($scope, AuthService) {
  $scope.user = {
    name: '',
    password: ''
  };
 
  $scope.login = function() {
    AuthService.login($scope.user).then(function(msg) {
      // $state.go('inside');
      console.log("logged in.");
    }, function(errMsg) {
      console.log("log in failed.");
    });
  };
})
 
.controller('RegisterCtrl', function($scope, AuthService) {
  $scope.user = {
    name: '',
    password: ''
  };
 
  $scope.signup = function() {
    AuthService.register($scope.user).then(function(msg) {
      // $state.go('outside.login');
      console.log("sucessful registration.");
 
    }, function(errMsg) {
      console.log("sign up failed.");
 
    });
  };
})
 
.controller('InsideCtrl', function($scope, AuthService, API_ENDPOINT, $http) {
  $scope.destroySession = function() {
    AuthService.logout();
  };
 
  $scope.getInfo = function() {
    $http.get(API_ENDPOINT.url + '/memberinfo').then(function(result) {
      $scope.memberinfo = result.data.msg;
    });
  };
 
  $scope.logout = function() {
    AuthService.logout();
    // $state.go('outside.login');
  };
})
 
.controller('AppCtrl', function($scope, $ionicPopup, AuthService, AUTH_EVENTS) {
  $scope.$on(AUTH_EVENTS.notAuthenticated, function(event) {
    AuthService.logout();
    // $state.go('outside.login');
    var alertPopup = $ionicPopup.alert({
      title: 'Session Lost!',
      template: 'Sorry, You have to login again.'
    });
  });
});


.controller("ListCtrl", function ($scope, $http, $filter, $cookies) {

      $scope.featuredArticle = null;
      $scope.fullArticles = [];
      $scope.lastArticle = {};
      $scope.infinite_articles = [];
      $scope.random = true;
      $scope.page_position = 0;


/*Infinte Scroll*/
/*Infinte Scroll*/
/*Infinte Scroll*/

    var goal = 0;
    var reached = false;
    var window_height = parseInt(window.innerHeight);
    var last_scroll = 0;
    var posts_container;


    checkScrollDist = function() {
      if($scope.fullArticles[0] == null){
        posts_container = $( "#page-container" );
      }else{
        posts_container = $( "#main-content" );
      }
      var goal = posts_container.offset().top + posts_container.outerHeight(true) - ( window_height * 2 );


      var scroll = $(window).scrollTop();
      if (scroll > last_scroll){
        if(scroll >= goal && reached == false){

          reached = true;
          //get the next published article
          if($scope.random == true){
            $http.get("API_ENDPOINT/v1/articles/any/2016-1-1, 2016-5-1").then(function( res ){
              article = res.data;
              if(typeof(article) !== 'undefined'){
                //push the article onto the infinite articles array
                $scope.infinite_articles.push(article);

                if($scope.fullArticles[0] == null){
                  posts_container = $( "#page-container" );
                }else{
                  $scope.fullArticles.push(article);
                  posts_container = $( "#main-content" );
                }

                $scope.lastArticle = article;
                window_height = parseInt(window.innerHeight);
                goal = posts_container.offset().top + posts_container.outerHeight(true) - ( window_height * 2 );
              }else{
                $("#loader").fadeOut();
                $("#timeline-end").fadeIn();
              }
              reached = false;
              addFaceBookWidgets();
            });
          }else{
            $http.get("API_ENDPOINT/v1/articles/next/" + String($scope.lastArticle._id) ).then(function( res ){
              article = res.data[0];
              if(typeof(article) !== 'undefined'){
                //push the article onto the infinite articles array
                $scope.infinite_articles.push(article);

                if($scope.fullArticles[0] == null){
                  posts_container = $( "#page-container" );
                }else{
                  $scope.fullArticles.push(article);
                  posts_container = $( "#main-content" );
                }

                $scope.lastArticle = article;
                window_height = parseInt(window.innerHeight);
                goal = posts_container.offset().top + posts_container.outerHeight(true) - ( window_height * 2 );
              }else{
                $("#loader").fadeOut();
                $("#timeline-end").fadeIn();
              }
              reached = false;
              addFaceBookWidgets();

            });
          }//end else

        }//end if

      } else {
         // upscroll code
      }

      last_scroll = scroll;
    };//end checkScrollDist


    $(window).scroll(checkScrollDist);

/*Infinte Scroll*/
/*Infinte Scroll*/
/*Infinte Scroll*/

//get articles
      //when the window loads, ajax in a set of articles
      $http.get("API_ENDPOINT/v1/articles/5").then(function( res ){
        $scope.infinite_articles = res.data;
        $scope.lastArticle = $scope.infinite_articles[$scope.infinite_articles.length - 1];
      });

      $scope.getRandomArticle = function(range){
        // range ex. '2016-1-1, 2016-5-1'
        var range = typeof(range) === "undefined" ? "" : range;
        $http.get("API_ENDPOINT/v1/articles/any/" + range).then(function( res ){
          console.log(res.data.title);
          return res.data;
        });
      };

      $scope.loadNewArticles = function(){
        $http.get("API_ENDPOINT/v1/articles/3").then(function( res ){
          $scope.infinite_articles = res.data;
          $scope.newArticleCount = 0;
          // console.log(res.data);
        });
      };//end loadNewArticles

      $scope.getRelatedArticles = function(article){
        $http.get("API_ENDPOINT/v1/related/article.slug/3").then(function( res ){
          $scope.articles = res.data;
          // console.log(res.data);
        });
      };//end getRelatedArticles

      $scope.openArticle = function(article){
        $scope.page_position = window.pageYOffset;//get current position of page before the article is opened.
        $http.get("API_ENDPOINT/v1/"+ article.slug).then(function( res ){
          $scope.fullArticles[0] = res.data;
          $("html, body").animate({ scrollTop: 0 }, "fast");//scroll to the top of the page automatically. (500 so we scroll past the featured article)
          //push this article to the browser history so the back button puts the user at their last activity
          history.pushState($scope.fullArticles[0], $scope.fullArticles[0].title, $scope.fullArticles[0].slug);
          //dynamically change the site's document title
          document.title = $scope.fullArticles[0].title + " - Blavity";
          //store this article id in the viewedArticles array
          if(typeof(window.localStorage !== 'undefined')){
            //get the value of viewedArticles, parse into json object, push article id into the object
            var viewedArticles = typeof(window.localStorage.getItem('viewedArticles')) === "string" ? JSON.parse(window.localStorage.getItem('viewedArticles')) : {};
            viewedArticles.articles = typeof(viewedArticles.articles) !== 'undefined' && viewedArticles.articles.length > 0 ? viewedArticles.articles : [];
            viewedArticles.articles.push( { id : article._id, slug : article.slug});
            window.localStorage.viewedArticles = JSON.stringify(viewedArticles);
            console.log(window.localStorage.viewedArticles);
            // window.localStorage.removeItem("viewedArticles");
            // console.log(window.localStorage.viewedArticles);

          }//end if browser supports localstorage

        });
      };//end openArticle

      $scope.closeArticle = function(article){
        // window.scrollTo(0, $scope.page_position);//scroll back to where the page was before we opened an article
        $("html, body").animate({ scrollTop: $scope.page_position }, "fast");
        $scope.fullArticles = [];
        document.title = "Blavity";
        history.pushState(article, "Blavity", "/");
      };//end closeArticle

      $scope.startWithArticle = function(slug){
        $scope.page_position = window.pageYOffset;//get current position of page before the article is opened.
        $("html, body").animate({ scrollTop: 0 }, "fast");//scroll to the top of the page automatically. (500 so we scroll past the featured article)
        $http.get("API_ENDPOINT/v1"+ slug).then(function( res ){
          $scope.fullArticles[0] = res.data;
          history.pushState($scope.fullArticles[0], $scope.fullArticles[0].title, $scope.fullArticles[0].slug);
          document.title = "Blavity | " + $scope.fullArticles[0].title;
          console.log($scope.fullArticles[0].title + " Pushed to history");
        });
      };//end startWithArticle

      // console.log("pathname: "+ location.pathname);
      if(location.pathname.trim() == "/"){
      }else{
        $scope.startWithArticle(location.pathname);
      }//end else
});


.controller("AuthorCtrl", function ($scope, $http, $filter, $cookies) {
      $scope.theAuthor = null;

      $scope.openAuthor = function(author){
        $scope.page_position = window.pageYOffset;//get current position of page before the article is opened.
        $scope.theAuthor = author;
        $("html, body").animate({ scrollTop: 0 }, "fast");//scroll to the top of the page automatically. (500 so we scroll past the featured article)
        //push this article to the browser history so the back button puts the user at their last activity
        history.pushState($scope.theAuthor, $scope.theAuthor.username, $scope.theAuthor.username);
        //dynamically change the site's document title
        document.title = $scope.theAuthor.username + " on Blavity";
      };//end openArticle
});

.controller("SearchCtrl", function ($scope, $http, $cookies) {
      $scope.searchResults = [];

      $scope.searchArticles = function(search){
        $http.get(API_ENDPOINT.url + '/v1/articles/search/' + search).then(function(response){
          $("#mobile-nav").slideUp();
          $("#loader").fadeOut();
          $("html, body").animate({ scrollTop: 0 }, "fast");//scroll to the top of the page automatically. (500 so we scroll past the featured article)

          if(response.data.hits.length > 0){
            $scope.infinite_articles = [];
            for (var i = 0; i < response.data.hits.length; i++) {
              $scope.infinite_articles.push(response.data.hits[i].document);
            }
          }

          $("#search-results-end").fadeIn();

          // console.log(response.data.hits);
        });//end post to new article
      };//end searchArticles
});


.controller("ArticleEditCtrl", function ($scope, $http, $filter, Upload, $cookies) {
      $scope.editMode = false;

      $scope.closeSubmissionForm = function(){
        $scope.article = null;
      };//end closeSubmissionForm

      $scope.toggleEditMode = function(){
        $scope.editMode = !$scope.editMode;
        $("#mobile-nav").slideUp();
        // CKEDITOR.inlineAll();
        $('body').contents().find("*[contenteditable]").each(function() {
          var element = $(this);
          if($scope.editMode === false){
            element.attr("contenteditable", "false");
            element.removeClass("editable");
          }else{
            element.attr("contenteditable", "true");
            element.addClass("editable");
          }
        });


        $('*[contenteditable]').each(function(){
            $(this).html($(this).html().replace(/&nbsp;/gi,'').replace(/&amp;/gi,'').replace(/&lt;/gi,'').replace(/&gt;/gi,'').replace(/&quot;/gi,'').replace(/&copy;/gi,''));

        });

      }; //end toggleEditMode

      $scope.saveArticleEdit = function(article){
        //turn tags into array
        console.log(article.tags);
        if(article.tags.length > 0){
        }else if(article.tags.length > 2){
          alert("You can only add up to 3 tags!");
          return;
        }else{
          article.tags = typeof(article.tags) === 'undefined' ? "" : article.tags.split(/\s*,\s*/);
        }

        $http.put(API_ENDPOINT.url + '/v1/articles/', {article: article} ).then(function(respo){
          // socket.emit('article edited', {article: article});
          $scope.editMode = false;
          //if you want to update the main list everytime an article is edited. (live editing)
          //but it will cause you to lose your place in infiite scroll since that's actually a different list
          // $http.get("API_ENDPOINT/v1/articles/10").then(function( res ){
          //   $scope.articles = res.data;
          //   console.log(res.data);
            $("#submission-form-container").fadeOut();
            $scope.article = null;
            $('body').contents().find("*[contenteditable='true']").each(function() {
              $(this).attr("contenteditable", "false");
                $(this).removeClass("editable");
            });
          // });//end post
        });
      };//end saveArticleEdit


      $scope.publishArticle = function(article){
        //toggle the article's pusblished state
        article.published = !article.published;
        //then just send as an update on this article
        $http.put(API_ENDPOINT.url + '/v1/articles/', {article: article} ).then(function(response){

          var article = response.data;

          $scope.npArticlesLoaded.push(article._id);

          $scope.editMode = false;

          if(article.published == true){
            socket.emit('article published', {article: article});
          }//end if article is published

        });
      };//end publishArticle

      //saves the article by posting it to the api save endpoint in the correct format
      $scope.createArticle = function(article){
        //turn tags into array
        if(article.tags.length > 0){
        }else{
          article.tags = typeof(article.tags) === 'undefined' ? "" : article.tags.split(/\s*,\s*/);
        }
        // console.log(article.tags);

        $http.post(API_ENDPOINT.url + '/v1/articles', {article: article}).then(function(response){

            //emit event to everyone except the person who posted it.
            socket.emit('article created', {article: article});
            //empty the (new) article variable
            $("#submission-form-container").fadeOut();
            $scope.article = null;

        });//end post to new article
      };//end createArticle

      $scope.deleteArticle = function(article){

        var cnf = confirm("Are you sure you want to delete this article?");

        if(cnf){
          $http.delete(API_ENDPOINT.url + '/v1/articles/' + article._id ).then(function(respo){
            // socket.emit('article deleted', {article: article});
            var index = $scope.infinite_articles.indexOf(article);
            $scope.infinite_articles.splice(index, 1);
          });
        }else{
          //do nothing
        }

      };//end deleteArticle
});


.controller("CommentsCtrl", function ($scope, $http, $filter, $cookies) {
      $scope.loadComments = function(article){

        $http.get("API_ENDPOINT/v1/articles/comments/" + article._id + "/20").then(function( res ){
          console.log(res.data);
          article.comments = (res.data);
        });
      };

      for (var i = $scope.fullArticles.length - 1; i >= 0; i--) {
        $scope.loadComments($scope.fullArticles[i]);
      };

      //saves the comment by posting it to the api save endpoint in the correct format
      $scope.saveComment = function(article){
        var comment = {};
        comment.body = article.newComment;
        comment.created_at = Date.now();
        comment._article = article._id;
        article.commentCount += 1;
        article.comments.unshift(comment);


        $http.post(API_ENDPOINT.url + '/v1/articles/comments', {comment: comment}).then(function(response){
          console.log(response);
          return;

            //emit event to everyone except the person who posted it.
            if(response == "Unauthorized"){
              var index = article.comments.indexOf(comment)
              article.comments.splice(index, 1);
              console.log(article);
            }
            else if(response.data.code && response.data.code == 100){
              socket.emit('comment created', {comment: comment});

            }else{
              var index = article.comments.indexOf(comment)
              article.comments.splice(index, 1);
              console.log(article);
            }

        });//end post to new article
        //empty the (new) comment variable
        article.newComment = null;

      };//end saveComment


      $scope.deleteComment = function(comment){

        var cnf = confirm("Are you sure you want to delete this comment?");

        if(cnf){
          $http.delete(API_ENDPOINT.url + '/v1/articles/comments/' + article._id ).then(function(respo){
            // socket.emit('article deleted', {article: article});
            var index = $scope.infinite_articles.indexOf(article);
            $scope.infinite_articles.splice(index, 1);
          });
        }else{
          //do nothing
        }

      };//end deleteArticle

});


    .controller("ArticleCtrl", function ($scope, $http, $filter, Upload, $cookies) {


      $scope.newArticleCount = 0;
      $scope.user = null;
      $scope.user = {email:"test@test.com", password: "test"};
      $scope.npArticlesLoaded = [];

//sockets - realtime updates
      //open up a socket.io for emitting and listening to events through sockets
      var socket = io('http://45.55.30.92:3000');
      //when the acrticle created event is detected
      socket.on('article created', function(resp){
        // $scope.$apply();//so angular will refresh the view and add our flash message
      });//end on article published

      socket.on('article published', function(resp){
        console.log(resp.article._id);
        $scope.npArticlesLoaded = [];
        $scope.$apply();//so angular will refresh the view and add our flash message
        //get the article that triggered the event using the api by it's slug
        // $http.get(API_ENDPOINT.url + '/v1/' + resp.article.slug ).then(function(rsp){
        //   $scope.infinite_articles.push(rsp.data);
        //   console.log(rsp.data);
        // });//end get article by slug
      });//end on article published
//end sockets

      window.onpopstate = function(event) {
        // alert("location: " + document.location + ", state: " + JSON.stringify(event.state));
        $scope.closeArticle(event.state);
        $scope.fullArticles = [];
        $scope.$apply();
      };

//authentication

      $scope.signUp = function(user){
        $http.post("API_ENDPOINT/signup", {user:user}, function( res ){
          console.log(res.data);
        });
      };

      $scope.signIn = function(user){
        console.log("user: "+ user.email);

        $http.post("API_ENDPOINT/authenticate", {user:user}, function( res ){
          console.log(res);
          // console.log(res.data);
          // console.log(res.data.success);
          // console.log(res.data.token);
          //
          // if(res.data.sucess == true){
          //   $cookies.put('token', res.data.token);
          //   console.log(res.data.token);
          //   console.log($cookies.getAll());
          // }else{
          //   alert("Sorry, sign in failed for some reason!");
          //   window.location = "http://jordancryingface.com";
          // }
        });
      };

//end authentication


      $scope.likeArticle = function(article){
        //make likes an empty array if one doesn't already exist on the article
        article.likes = typeof(article.likes === 'undefined') ? [] : article.likes;
        article.likes.push("user1234");
        //todo://push the actual user id
        return;

      };


      $scope.bookmarkArticle = function(article){
        if(typeof(window.localStorage !== 'undefined')){
          //get the value of viewedArticles, parse into json object, push article id into the object
          var bookmarkedArticles = typeof(window.localStorage.getItem('bookmarkedArticles')) === "string" ? JSON.parse(window.localStorage.getItem('bookmarkedArticles')) : {};
          bookmarkedArticles.articles = typeof(bookmarkedArticles.articles) !== 'undefined' && bookmarkedArticles.articles.length > 0 ? bookmarkedArticles.articles : [];
          bookmarkedArticles.articles.push( { id : article._id, slug : article.slug});
          window.localStorage.bookmarkedArticles = JSON.stringify(bookmarkedArticles);
          console.log(window.localStorage.bookmarkedArticles);
        }
      };


      // //format slugs automatically
      // $scope.formatSlug = function(title){
      //   return title.toLowerCase().replace(/\s+/g, "-"); //make string lowercase
      //   $scope.apply();
      // };

      // $scope.openEditArticle = function(article, event){
      //   $("#submission-form-container").fadeIn();
      //   var article_top = event.pageY + "px";
      //   if(parseInt(screen.width) <= 400){//if on mobile
      //     //just use css to open the form at the top left absolute
      //     $("#submission-form-container").css("top", $(window).scrollTop());

      //   }else{
      //     $("#submission-form-container").css("top", article_top);
      //   }//end else
      //   $scope.article = article;
      //   $scope.article.tags = article.tags.toString(); //turn tags to back into a comma delim string when editing
      //   // $scope.article.slug = $scope.formatSlug(article.title);//format the slug based on the title

      // };//end openEditArticle


      //autoformat the slugs from the title
      $('#slug').bind('keyup keypress blur', function() {
          var slug = $(this).val();
          slug = slug.toLowerCase().replace(/\s+/g, "-"); //replace spaces with dashes

          // $('#slug').val(slug);
          $scope.article.slug = slug;
      });

      $('#title').bind('keyup keypress blur', function() {
          var title = $(this).val();
          slug = title.toLowerCase().replace(/\s+/g, "-"); //replace spaces with dashes
          // $('#slug').val(slug);
          $scope.article.slug = slug;

      });

    });//end article controller







