var fs = require('fs');
var _ = require('lodash');
var appAbsPath = __dirname;
var config = require('./production');

if (process.env.NODE_ENV && process.env.NODE_ENV.length > 0) {
    try {
        fs.accessSync(appAbsPath + '/' + process.env.NODE_ENV + '.js', fs.F_OK);
        console.info('Override config by "' + process.env.NODE_ENV + '"');
        _.merge(config, require('./' + process.env.NODE_ENV + '.js'));
        config.configName = process.env.NODE_ENV;
    } catch (e) {
        console.warn('Configuration for "' + process.env.NODE_ENV + '" NOT FOUND. Used DEFAULT');
    }
} else {
    console.info('Using PRODUCTION config');
}

try {
    fs.accessSync(appAbsPath + '/local.js', fs.F_OK);
    console.info('Use local config file');
    _.merge(config, require('./local.js'));
    config.configName = 'local';
} catch (e) {

}

module.exports = config;