var express     = require('express');//server
var app         = express();
var http        = require('http').Server(app);
var io          = require('socket.io')(http);//sockets
var request     = require('request');//requests
var morgan      = require('morgan');//logging
var moment = require('moment');
var shortcode = require('shortcode-parser');
var port        = process.env.PORT || 8080;
var apiUrl="http://api.blavity.com/v1/";
//var apiUrl="http://localhost:3100/v1/";
shortcode.add('caption', function(buf, opts) {
  if (opts.upper) buf = buf.toUpperCase();
  return  buf;
});

//use ejs as the view engine
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');

//for serving static pages
/*
 __ _        _   _          ___
/ _\ |_ __ _| |_(_) ___    / _ \__ _  __ _  ___  ___
\ \| __/ _` | __| |/ __|  / /_)/ _` |/ _` |/ _ \/ __|
_\ \ || (_| | |_| | (__  / ___/ (_| | (_| |  __/\__ \
\__/\__\__,_|\__|_|\___| \/    \__,_|\__, |\___||___/
                                     |___/
*/
app.use(express.static(__dirname +'/../public'));
//app.use('/', express.static('public'));

//log to the console
app.use(morgan('dev'));
//
//
//
//home
app.get('/', function(req, res){
    //for middleware
    request.get(apiUrl+'articles/16', function (error, response, articles) {
      if (!error && response.statusCode == 200) {

        var articles = JSON.parse(response.body);
          res.render('new/home-demo', { articles: articles,moment: moment  });
      }else{
        res.render('notfound');
      }//end else
    });
    //end api call
});

/*--------LOGIN PAGE------------*/
app.get('/login', function(req, res){
  res.render('new/login');
});

/*--------register PAGE------------*/
app.get('/register', function(req, res){
  res.render('new/register');
});

/*--------register PAGE------------*/
app.get('/author', function(req, res){
  res.render('new/author');
});


/*--------Forget password PAGE------------*/
app.get('/forgotpassword', function(req, res){
  res.render('new/forgot_password');
});

app.get('/post', function(req, res){

  res.render('new/new_post');
})

//blog
app.get('/blog', function(req, res){
    //for middleware
    request.get(apiUrl+'articles/16', function (error, response, articles) {
      if (!error && response.statusCode == 200) {

        var articles = JSON.parse(response.body);

        res.render('blog', { articles: articles });

      }else{
        // res.json({message: "retrieval error!"});
        res.render('notfound');

      }//end else
    });
    //end api call
});


//submissions
app.get('/submit', function(req, res){
    res.render('submissions');
});


//single article
app.get('/:slug', function(req, res){
    //for middleware
    request.get(apiUrl+req.params.slug, function (error, response, articles) {
      if (!error && response.statusCode == 200) {

        var article = JSON.parse(response.body);

        if( !article ){
          res.render('notfound');
        }else{
          res.render('new/single', { article: article,moment:moment,shortcode:shortcode });
        }


      }else{
        res.render('notfound');
      }//end else
    });
    //end api call
});
//end article
//
//
//


//end static pages



//listen
var server = http.listen(3000, function(){
  console.log('listening on *:3000');
});

server.timeout = 3000;
//end listen
