var express     = require('express');//server
var app         = express();
var http        = require('http').Server(app);
var io          = require('socket.io')(http);//sockets
var request     = require('request');//requests
var morgan      = require('morgan');//logging
var moment      = require('moment');
var shortcode   = require('shortcode-parser');
var md5         = require('md5');
var config      = require('../config/config');
//var apiUrl      = "http://api.blavity.com/v1/";
var apiUrl      = config.api_url;

shortcode.add('caption', function(buf, opts) {
  if (opts.upper) buf = buf.toUpperCase();
  return  buf;
});


//use ejs as the view engine
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');

//for serving static pages
/*
 __ _        _   _          ___
/ _\ |_ __ _| |_(_) ___    / _ \__ _  __ _  ___  ___
\ \| __/ _` | __| |/ __|  / /_)/ _` |/ _` |/ _ \/ __|
_\ \ || (_| | |_| | (__  / ___/ (_| | (_| |  __/\__ \
\__/\__\__,_|\__|_|\___| \/    \__,_|\__, |\___||___/
                                     |___/
*/
app.use(express.static(__dirname +'/../public'));
//app.use('/', express.static('public'));

//log to the console
app.use(morgan('dev'));
//
//
//
//home
app.get('/', function(req, res){
    //get data from api
    request.get(apiUrl+'articles/16', function (error, response, articles) {
      if (!error && response.statusCode == 200) {
        var articles = JSON.parse(response.body);
        console.log(articles.length);
        return  res.render('home', { page: "home", articles: articles, moment: moment, md5:md5 });
      }else{
        res.redirect('/404');
      }//end else
    });
    //end api call
});

//static pages

//about us
app.get('/about', function(req, res){
  res.render('about', {});
});

//about us
app.get('/terms-of-service', function(req, res){
  res.render('terms', {});
});

//privacy
app.get('/privacy', function(req, res){
  res.render('privacy', {});
});

//advertisng
app.get('/advertisng', function(req, res){
  res.render('advertisng', {});
});

//jobs
app.get('/jobs', function(req, res){
  res.render('jobs', {});
});

//submit-content (legacy google-forms submissions until we unleash the beash)
app.get('/submit-content', function(req, res){
  res.render('submit-content', {});
});

//end static pages


//categories
app.get('/404', function(req, res){

  var categories = 'blerd,lifestyle,news,culture,opinion';
    //get data from api
    request.get(apiUrl+'articles/categories/' + categories + '/15/0', function (error, response, articles) {
      if (!error && response.statusCode == 200) {

        var articles = JSON.parse(response.body);
          res.render('notfound', { page: "404", articles: articles, moment: moment, md5:md5 });
      }else{

      }//end else
    });
    //end api call
});


//categories
app.get('/categories/:category', function(req, res){

  var category = req.params.category;
    //get data from api
    request.get(apiUrl+'articles/categories/' + category + '/17/0', function (error, response, articles) {
      if (!error && response.statusCode == 200) {

        var articles = JSON.parse(response.body);
          res.render('categories', { page: "categories", category: category, articles: articles, moment: moment, md5:md5 });
      }else{
        res.redirect('/404');
      }//end else
    });
    //end api call
});

//tags
app.get('/tags/:tag', function(req, res){

  var tag = req.params.tag;
    //get data from api
    request.get(apiUrl+'articles/tags/' + tag + '/17/0', function (error, response, articles) {
      if (!error && response.statusCode == 200) {

        var articles = JSON.parse(response.body);
          res.render('categories', { page: "tags", category: tag, articles: articles, moment: moment, md5:md5 });
      }else{
        res.redirect('/404');
      }//end else
    });
    //end api call
});

//videos
app.get('/videos', function(req, res){

    //get data from api
    request.get(apiUrl+'articles/type/video/16/0', function (error, response, articles) {
      if (!error && response.statusCode == 200) {

        var articles = JSON.parse(response.body);
          res.render('videos', { page: "videos", articles: articles, moment: moment, md5:md5 });
      }else{
        res.redirect('/404');
      }//end else
    });
    //end api call
});

/*--------LOGIN PAGE------------*/
app.get('/login', function(req, res){
  res.redirect('/');
  //res.render('login');
});

/*--------register PAGE------------*/
app.get('/register', function(req, res){
  res.redirect('/');
  //res.render('register');
});

app.get('/mail/verification/:token', function(req, res){
  request.get(apiUrl+'verification/' + req.params.token, function (error, response) {
    if (!error && response.statusCode == 200) {
      console.log(response.body);
      var verification = JSON.parse(response.body);
      res.render('login', { answerSuccess: verification.success, answerMessage: verification.msg });
    }
  });
});
app.get('/mail/reset-password/:token', function(req, res){
    res.render('forgot_password', {resetToken: req.params.token});

  //request.get(apiUrl+'reset-password/' + req.params.token, function (error, response) {
  //  if (!error && response.statusCode == 200) {
  //    console.log(response.body);
  //    var reset = JSON.parse(response.body);
  //    res.render('new/login', { answerSuccess: reset.success, answerMessage: reset.msg });
  //  }
  //});
});

/*--------author PAGE------------*/
app.get('/author/:slug', function(req, res){

  request.get(apiUrl+'articles/authors/'+req.params.slug+'/'+3, function (error, response, articles) {
    if (!error && response.statusCode == 200) {
      var articles = JSON.parse(articles);
      //console.log(articles.slice(0, 3));
      if( !articles ){
        res.redirect('/404');
      } else{
        request.get(apiUrl+'authors/'+req.params.slug,function(error, response, authors){
          var authors = JSON.parse(authors);
          console.log(authors);

            if (!error && response.statusCode == 200) {
              console.log(authors);
              if(authors){
                //console.log(authors);
                res.render('author',{page: "author", articles:articles.slice(0, 3),md5:md5,moment: moment,authors:authors,totalpost:articles.length});
              }
              else res.redirect('/404');
            }
            else{
              res.redirect('/404');
            }
        })

      }
    }else{
      res.redirect('/404');
    }//end else
  });

});


/*--------Forget password PAGE------------*/
app.get('/forgotpassword', function(req, res){
  res.redirect('/');
  //res.render('forgot_password');
});

app.get('/post', function(req, res){
  res.redirect('/');
  //res.render('new_post');
});



//submissions
app.get('/submit', function(req, res){
    res.render('submissions');
});


//single article
app.get('/:slug', function(req, res){
    //get data from api
    request.get(apiUrl+req.params.slug, function (error, response, articles) {
      if (!error && response.statusCode == 200) {

        var article = JSON.parse(response.body);

        var artcls;

        if(article){
          // if this post has a featured video
          var template = (typeof article.videourl !== "undefined" && article.videourl != 'null') ? "single-video" : "single";
          //use the single-video page, othewise just use single
          // console.log("template: ");
          // console.log(template);
        }

        // var template = 'single';

        request.get(apiUrl+'articles/4', function (error, response, articles) {
          if (!error && response.statusCode == 200) {

            artcls = JSON.parse(response.body);
            // console.log(artcls)
            if( !article ){
              res.redirect('/404');
            }else{
              res.render(template, { page: "single", article: article,articles: artcls,moment:moment,shortcode:shortcode, md5:md5 });
            }

          }else{
          }//end else
        });


      }else{
        res.redirect('/404');
      }//end else
    });
    //end api call
});
//end article
//
//
//


//end static pages



//listen
var server = http.listen(config.port, function(){
  console.log('listening on *:' + config.port);
});

server.timeout = 3000;
//end listen
