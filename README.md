MEAN project
# blog-ui
A User interface for the blog which consumes the rest api 

1st, you must have node.js and mongo.db installed
To get started:

1. clone the repository
2. cd blog-ui
3. npm install -g
4. npm update -g
5. npm install forever -g
6. forever start index.js
7. You're good to go! (you can terminate the ssh connection and the ui will continue to run)